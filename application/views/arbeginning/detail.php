<?php 
$segment = $this->uri->segment('3');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>
      AR Beginning
    </h4>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Transaction</a></li>
      <li class="active">AR Beginning</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
     <div class="col-xs-8">
      <div class="box box-danger">
        <form class="form-horizontal" id="formData" name="formData"  method="post" action="" enctype="multipart/form-data">
         <!--  Hidden element -->
         <input type="hidden" value="<?php echo $detail['idtrans'];?>" id="idtrans" name="idtrans">
         <div class="box-header with-border">
           <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-6">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $detail['notrans'];?>" class="form-control" id="transno" name="transno" disabled="disabled">
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                  <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                   <input type="text" class="form-control pull-right" id="datetrans" name="datetrans" data-toggle="tooltip" data-placement="transdate" data-date-format="dd/mm/yyyy" value="<?php echo $detail['datetrans'];?>" onchange="saveprint();">
                 </div><!-- /.input group -->
               </div>
             </div>
             <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Customer</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="customerid" id="customerid" onchange="saveprint();">
                  <option value="<?php echo $detail['customerid'];?>"><?php echo $detail['customername'];?></option>
                  <?php foreach($customer as $customers):?>
                    <option value="<?php echo $customers->idcustomer;?>"><?php echo $customers->customername;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Currency</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="currency" id="currency" onchange="saveprint();">
                  <option value="<?php echo $detail['currencyname'];?>"><?php echo $detail['currencyname'];?></option>
                  <?php foreach($currency as $currencys):?>
                    <option value="<?php echo $currencys->currencyname;?>"><?php echo $currencys->currencyname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div> 
             <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Account</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="accountid" id="accountid" onchange="saveprint();">
                  <option value="<?php echo $detail['accountid'];?>"><?php echo $detail['accountname'];?></option>
                  <?php foreach($account as $accounts):?>
                    <option value="<?php echo $accounts->idaccount;?>"><?php echo $accounts->accountname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">TOP</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="top" id="top" onchange="saveprint();">
                  <option value="<?php echo $detail['top'];?>"><?php echo $detail['top'];?></option>
                  <?php foreach($top as $tops):?>
                    <option value="<?php echo $tops->topname;?>"><?php echo $tops->topname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>  
          </div>
          <!-- Coloumn 2-->   
          <div class="col-md-6">
         
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Invoice Ref</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo $detail['invoiceref'];?>" class="form-control" id="invoiceref" name="invoiceref" onchange="saveprint();">
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Due Date</label>
              <div class="col-sm-9">
              <input type="text" value="<?php echo $detail['duedate'];?>" class="form-control" id="duedate" name="duedate" onchange="saveprint();">
              </div>
            </div>  
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Rate</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo $detail['rate'];?>" class="form-control" id="rate" name="rate" onchange="saveprint();">
              </div>
            </div>  

            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Begin Amount</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo $detail['beginamt'];?>" class="form-control" id="beginamt" name="beginamt" onchange="saveprint();">
              </div>
            </div>  
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Remark</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="remark" name="remark" onchange="saveprint();"><?php echo $detail['remark'];?></textarea>
              </div>
            </div>
          </div> 
        </div>
      </div><!-- /.box-header --> 
    </div>   
    <div class="box-footer" style="margin-top:-20px !important">
     <?php
     if($detail['status']==0) {
      $btnstatus = 'Released';
      $btnclass = 'fa fa-arrow-right';
    }elseif ($detail['status']==9) {
      $btnstatus = 'Reopen';
      $btnclass = 'fa fa-arrow-left';
    }else{
      $btnstatus = 'Open';
      $btnclass = 'fa fa-arrow-left';
    };
    ?>
    <button class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow')"><i class="<?php echo $btnclass; ?>"></i> <?php echo $btnstatus; ?></button> 
    <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveprint();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a>
    <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a>
    <a href="<?php echo site_url('arinvoice');?>" type="button" class="btn btn-default btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
    <iframe src='<?php echo site_url('arinvoice/detail/8');?>"' height="0" width="0" frameborder='0' name="print_frame"></iframe>
  </div>  
</form>
</div>
</section><!-- /.content -->
</div>
</div>
</section>

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid">
          <option ondblclick="window.open('',target='_blank')" value="arinvoice">AR Beginning</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-flat" onclick="showslip()"> <i class="fa fa-search"></i> Preview</button>
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- /.javascript -->
<script type="text/javascript">  
function doSearch(){
  $('#dg').datagrid('load',{
    msearchdetail: $('#msearchdetail').val(),
  });
}

function keyCode(event) {
  var x = event.keyCode;
  if (x == 13) {
    doSearch();
  }
  return false;
}
$('#formData').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

function refreshgrid()
{
  $('#dg').datagrid('reload'); 
}
function expandAllRow()
{
  var dg = $('#dg');
  var count = dg.datagrid('getRows').length;
  for(var i=0; i<count; i++){
    dg.datagrid('expandRow',i);
  }
}
function showslip()
{
 var slipid = document.getElementById("slipid").value;
 var url = "report.php?mod=" + slipid + "&key="+ key;
 PopupCenterDual(url,'Popup_Window','1000','650');
}

function saveprint()
{
  var url;
  url = "<?php echo site_url('arbeginning/saveprint')?>";

    // ajax adding data to database
    $.ajax({
      url : url,
      type: "POST",
      data: $('#formData').serialize(),
      dataType: "JSON",
      success: function(data)
      {
             // alert("success!");
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            $.alert({
              type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error adding or update data!',
              });
          }
        });
  }

  function canceltransaction()
  {
    var url;
    url = "<?php echo site_url('arinvoice/canceltransaction')?>";

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this transaction <b><?php echo $detail['notrans'];?> </b>?',
      type: 'orange',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () {

         }
       },
       confirm: {   
        text: 'CANCEL',
        btnClass: 'btn-orange',
        action: function () {

          $.ajax({
            url : url,
            type: "POST",
            data: $('#formData').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                 // alert("success!");
               },
               error: function (jqXHR, textStatus, errorThrown)
               {
                $.alert({
                  type: 'orange',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error canceling data!',
                              });

              }
            });
        }
      },

    }
  });
  }

</script>
