<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <div id="type"></div>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Index</a></li>
        <li class="active">Welcome</li>
      </ol>
    </section> 

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Data has not been validated</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Transaction</th>
                    <th>Module</th>
                    <th>Wait to Validate</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="index.php?mod=deliveryorder">Delivery Order</a></td>
                    <td>Sales</td>

                    <td>4</td>
                    <td>34,800,000</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=arinvoice">AR Invoice</a></td>
                    <td>Account Receivable</td>

                    <td>6</td>
                    <td>6,800,000</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=arreceive">AR Receive</a></td>
                    <td>Account Receivable</td>

                    <td>1</td>
                    <td>499,000</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=purchaseorder">Purchase Order</a></td>
                    <td>Procurement</td>

                    <td>0</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=goodsreceive">Goods Receive</a></td>
                    <td>Procurement</td>

                    <td>1</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=apinvoice">AP Invoice</a></td>
                    <td>Account Payable</td>

                    <td>0</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td><a href="index.php?mod=appayment">AP Payment</a></td>
                    <td>Account Payable</td>

                    <td>1</td>
                    <td>50,000</td>
                  </tr>
                </tbody>
              </table>
            </div><!-- /.table-responsive -->
          </div><!-- /.box-body -->

        </div><!-- /.box -->
      </div><!-- /.col -->

      <div class="col-md-6">
        <!-- USERS LIST -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">New Product</h3>
            <div class="box-tools pull-right">
              <span class="label label-danger">4 New Product</span>
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <ul class="users-list clearfix">

              <li>
              <img src="<?php echo base_url('assets/images/icon/default.png');?>" alt="Product Image">
                <a class="users-list-name" href="#">Stator Polisher</a>
                <span class="users-list-date">0000-00-00</span>
              </li>

              <li>
                <img src="<?php echo base_url('assets/images/icon/default.png');?>" alt="Product Image">
                <a class="users-list-name" href="#">Switch Polisher</a>
                <span class="users-list-date">0000-00-00</span>
              </li>

              <li>
                <img src="<?php echo base_url('assets/images/icon/default.png');?>" alt="Product Image">
                <a class="users-list-name" href="#">Cb Cup Polisher</a>
                <span class="users-list-date">0000-00-00</span>
              </li>

              <li>
                <img src="<?php echo base_url('assets/images/icon/default.png');?>" alt="Product Image">
                <a class="users-list-name" href="#">Portable Blower</a>
                <span class="users-list-date">0000-00-00</span>
              </li>

            </ul><!-- /.users-list -->
          </div><!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="index.php?mod=inventory" class="uppercase">View All Product</a>
          </div><!-- /.box-footer -->
        </div><!--/.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="ion ion-ios-lightbulb-outline"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Blank Widget</span>
            
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
      </div><!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-globe"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Total Notification</span>

            <span class="info-box-number">14</span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
      </div><!-- /.col -->
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Total Mailbox</span>

            <span class="info-box-number">2</span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
      </div><!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Total Users</span>

            <span class="info-box-number">3</span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
      </div><!-- /.col -->
    </div><!-- /.row -->

  </section><!-- /.content -->
</div><!-- /.container -->
</div><!-- /.content-wrapper -->