
<body class="hold-transition skin-blue fixed layout-top-nav" data-spy="scroll" data-target="#scrollspy">
<div class="wrapper">

<header class="main-header">
    <nav class="navbar navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header" style="margin-left: -50px !important">
          <!-- <a href="<?php echo site_url('dashboard');?>" class="navbar-brand"><b>ERP</b>System</a> -->
          <a href="<?php echo site_url('dashboard');?>"><img src="<?php echo base_url('assets/images/site/logoindex.png');?>" alt="" /> </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-building-o"></i> Master <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Customer</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Customer</a></li>
                    <li><a href="<?php echo site_url('shipto');?>">Shipment To</a></li>
                    <li><a href="<?php echo site_url('contacttype');?>">Contact Type</a></li>
                    <li><a href="<?php echo site_url('businesstype');?>">Business Type</a></li>
                    <li><a href="<?php echo site_url('customertype');?>"> Customer Type</a></li>
                  </ul>
                </li>
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Employee</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('employee');?>">Employee</a></li>
                    <li><a href="<?php echo site_url('salesman');?>"> Salesman</a></li>
                    <li><a href="<?php echo site_url('department');?>"> Department</a></li>
                    <li><a href="<?php echo site_url('position');?>"> Position</a></li>
                    <li><a href="<?php echo site_url('educationlevel');?>"> Education Level</a></li>
                    <li><a href="<?php echo site_url('religion');?>"> Religion</a></li>
                  </ul>
                </li> 
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Product</a>
                  <ul class="dropdown-menu"> 
                    <li><a href="<?php echo site_url('inventory');?>">Product</a></li>
                    <li><a href="<?php echo site_url('inventorytype');?>">Product Type</a></li>
                    <li><a href="<?php echo site_url('inventorybrand');?>">Product Brand</a></li>
                    <li><a href="<?php echo site_url('inventorygroup');?>">Product Group</a></li>
                    <li><a href="<?php echo site_url('inventorysize');?>">Product Size</a></li>
                    <li><a href="<?php echo site_url('inventorycolor');?>">Product Color</a></li>
                    <li><a href="<?php echo site_url('unit');?>">Unit</a></li>
                  </ul>
                </li> 
                <li><a href="<?php echo site_url('supplier');?>">Supplier</a></li>
                <li class="divider"></li>
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">COA</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('account');?>"> Account</a></li>
                    <li><a href="<?php echo site_url('accountlvl1');?>"> Account Level 1</a></li>
                  </ul>
                </li>
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Warehouse</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('warehouse');?>"> Warehouse</a></li>
                    <li><a href="<?php echo site_url('rack');?>"> Rack</a></li> 
                    <li><a href="<?php echo site_url('matusedtype');?>">Material Used Type</a></li>
                  </ul>
                </li>
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Marketing</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('area');?>"> Area</a></li>
                    <li><a href="<?php echo site_url('complainttype');?>"> Complaint Type</a></li> 
                    <li><a href="<?php echo site_url('expeditiontype');?>"> Expedition Type</a></li>
                    <li><a href="<?php echo site_url('salesreturnreason');?>"> Sales Return Reason</a></li>
                  </ul>
                </li> 
                <li><a href="<?php echo site_url('bank');?>">Bank Account</a></li> 
                <li><a href="<?php echo site_url('paytype');?>">Payment Type</a></li>
                <li><a href="<?php echo site_url('currency');?>">Curency</a></li>
                <li><a href="<?php echo site_url('top');?>">Term of Payment</a></li>
                <li><a href="<?php echo site_url('costcenter');?>">Cost Center</a></li>  
                <li class="divider"></li>
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Place</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('location');?>">Location</a></li>
                    <li><a href="<?php echo site_url('city');?>">City</a></li>
                    <li><a href="<?php echo site_url('country');?>">Country</a></li>
                  </ul>
                </li>
                <li><a href="<?php echo site_url('year');?>">Year</a></li> 
              </ul>
            </li>
 
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cart-plus"></i> Sales <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('salesorder');?>">Sales Order</a></li> 
                <li><a href="<?php echo site_url('deliveryorder');?>">Delivery Order</a></li>
                <li><a href="<?php echo site_url('deliveryshipment');?>">Delivery Shipment</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo site_url('salesreturn');?>">Sales Return</a></li>
                <li><a href="<?php echo site_url('salesvariance');?>">Sales Variance</a></li>
                <li><a href="<?php echo site_url('salesadvance');?>">Sales Advance</a></li>
                <li><a href="<?php echo site_url('customerpricelist');?>">Customer Price List</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calculator"></i> Procurement <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('customer');?>">Purchase Request</a></li>
                <li><a href="<?php echo site_url('bidding');?>">Bidding</a></li>
                <li><a href="<?php echo site_url('vendorselection');?>">Vendor Selection</a></li>
                <li><a href="<?php echo site_url('purchaseorder');?>">Purcashe Order</a></li>
                <li><a href="<?php echo site_url('goodreceive');?>">Good Receive</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo site_url('purchasereturn');?>">Purchase Return</a></li>
                <li><a href="<?php echo site_url('purchaseadvance');?>">Purchase Advance</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-archive"></i> Warehouse <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Material Out</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('materialrequest');?>">Material Request</a></li>
                    <li><a href="<?php echo site_url('materialused');?>">Material Used</a></li> 
                  </ul>
                </li>
                <li><a href="<?php echo site_url('stockopname');?>">Stock Opname</a></li>
                <li><a href="<?php echo site_url('inventoryadj');?>">Inventory Adjustment</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> AR <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('customer');?>">AR Invoice</a></li>
                <li><a href="<?php echo site_url('arreceive');?>">AR Receive</a></li>
                <li><a href="<?php echo site_url('arbeginning');?>">AR Beginning</a></li>
                <li><a href="<?php echo site_url('arnotes');?>">AR Notes</a></li>
                <li><a href="<?php echo site_url('chequereceivable');?>">Cheque Receivable</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> AP <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('customer');?>">AP Invoice</a></li>
                <li><a href="<?php echo site_url('apreceive');?>">AP Receive</a></li>
                <li><a href="<?php echo site_url('apbeginning');?>">AP Beginning</a></li>
                <li><a href="<?php echo site_url('apnotes');?>">AP Notes</a></li>
                <li><a href="<?php echo site_url('chequepayable');?>">Cheque Payable</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> Cashier <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('cashreceipt');?>">Cash Receipt</a></li>
                <li><a href="<?php echo site_url('cashpayment');?>">Cash Payment</a></li>
                <li><a href="<?php echo site_url('cashtransfer');?>">Cash Transfer</a></li>  
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Cash Advance</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('cashadvance');?>"> Cash Advence Request</a></li>
                    <li><a href="<?php echo site_url('shipto');?>"> Cash Advence Approval</a></li>
                    <li><a href="<?php echo site_url('contacttype');?>"> Cash Advence Settlement</a></li>
                    <li><a href="<?php echo site_url('businesstype');?>"> Cash Advence Return</a></li> 
                  </ul>
                </li>
                <li><a href="#">Daily Rate</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> Accounting <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo site_url('generaljournal');?>">General Journal</a></li>
                <li><a href="<?php echo site_url('fixedasset');?>">Fixed Asset</a></li>
                <li><a href="<?php echo site_url('disposalfixedasset');?>">Disposal Fixed Asset</a></li>
                <li><a href="<?php echo site_url('prepaid');?>">Prepaid Expense</a></li>
                <li><a href="<?php echo site_url('posting');?>">Posting</a></li>
                <li><a href="<?php echo site_url('depreciationtype');?>">Depreciation Type</a></li> 
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> Report <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Sales Order</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Sales Order Detail</a></li>
                    <li><a href="<?php echo site_url('customer');?>"> Sales Order by Area</a></li>
                    <li><a href="<?php echo site_url('shipto');?>">Sales Order by Customer</a></li>
                    <li><a href="<?php echo site_url('contacttype');?>">Sales Order by Invoice</a></li>
                    <li><a href="<?php echo site_url('businesstype');?>">Sales Order by Product</a></li>
                    <li><a href="<?php echo site_url('customertype');?>"> Sales Order by Product Brand</a></li> 
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Sales Return</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Sales Return Detail</a></li>
                    <li><a href="<?php echo site_url('customer');?>"> Sales Return by Area</a></li>
                    <li><a href="<?php echo site_url('shipto');?>">Sales Return by Customer</a></li>
                    <li><a href="<?php echo site_url('contacttype');?>">Sales Return by Invoice</a></li>
                    <li><a href="<?php echo site_url('businesstype');?>">Sales Return by Product</a></li>
                    <li><a href="<?php echo site_url('customertype');?>"> Sales Return by Product Brand</a></li>
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Procurement</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Purchase Order Detail</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Purchase Order by Supplier</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>">Purchase Order by Product</a></li> 
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Warehouse</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Stock Aging</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Stock Ending</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Stock Ledger</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Stock Movement</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Material Request Planning</a></li> 
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Cashier</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Cash Advance</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Cash Bank Ledger</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Giro Receipt</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Notes Payable</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Notes Receiveble</a></li> 
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Account Receivable</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Account Receivable Aging</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Account Receivable by Customer</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Account Receivable Ledger</a></li>
                    <li><a href="<?php echo site_url('businesstype');?>"> Account Receivable Outstanding</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Cost of Sales</a></li> 
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Account Payable</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Account Payable Aging</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Account Payable Ledger</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Account Payable Outstanding</a></li>  
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#"> Accounting</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('customer');?>"> Balance Sheet</a></li> 
                    <li><a href="<?php echo site_url('shipto');?>">Balance Sheet Detail</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Income Statement</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Income Statement Detail</a></li>  

                    <li><a href="<?php echo site_url('businesstype');?>"> Journal Listing</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Ledger</a></li>   
                    <li><a href="<?php echo site_url('businesstype');?>"> Prepaid and Amortization</a></li> 
                    <li><a href="<?php echo site_url('businesstype');?>"> Income Statement Detail</a></li>  

                    <li><a href="<?php echo site_url('businesstype');?>"> Fixed Asset and Depreciation</a></li> 
                  </ul>
                </li>


              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url(); ?>userListing">User List</a></li>
              </ul>
            </li>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
             

            <!-- Notifications Menu -->
            <li class="dropdown notifications-menu">
              <!-- Menu toggle button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- Inner Menu: contains the notifications -->
                  <ul class="menu">
                    <li><!-- start notification -->
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <!-- end notification -->
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- Tasks Menu -->
            <li class="dropdown tasks-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                  <!-- Inner menu: contains the tasks -->
                  <ul class="menu">
                    <li><!-- Task item -->
                      <a href="#">
                        <!-- Task title and progress text -->
                        <h3>
                          Design some buttons
                          <small class="pull-right">20%</small>
                        </h3>
                        <!-- The progress bar -->
                        <div class="progress xs">
                          <!-- Change the css width attribute to simulate progress -->
                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">20% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                  </ul>
                </li>
                <li class="footer">
                  <a href="#">View all tasks</a>
                </li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">Adm</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    Husni - <small></small>
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                     <a href="<?php echo base_url(); ?>loadChangePass" class="btn btn-default btn-flat">Change Password</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>