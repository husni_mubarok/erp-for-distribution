<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li>
            <a href="<?php echo site_url('dashboard');?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-wrench fa-fw"></i> Sales<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="<?php echo site_url('customer');?>">Customer</a>
                </li>
                <li>
                    <a href="<?php echo site_url('lead');?>">Leads</a>
                </li>
                <li>
                    <a href="<?php echo site_url('competitor');?>">Competitor</a>
                </li>
                <li>
                    <a href="<?php echo site_url('opportunity');?>">Opportunities</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li>
            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Marketing<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="<?php echo site_url('document');?>">Documents</a>
                </li>
                <li>
                    <a href="<?php echo site_url('cases');?>">Case</a>
                </li>
                <li>
                    <a href="<?php echo site_url('contact');?>">Contact</a>
                </li>
                <li>
                    <a href="<?php echo site_url('casecategory');?>">Case Categoty</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li>
            <a href="#"><i class="fa fa-files-o fa-fw"></i> Report<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="<?php echo site_url('incomingdeals');?>">Incoming Deals</a>
                </li>
                <li>
                    <a href="<?php echo site_url('dealmilestone');?>">Project by Milestone</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
    </ul>
</div>
<!-- /.sidebar-collapse -->