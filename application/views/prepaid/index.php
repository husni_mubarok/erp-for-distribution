
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>
      Prepaid Expense
    </h4>
    <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Master</a></li>
      <li class="#">Prepaid Expense</li>

      <li class="active"> Status : 
        <select name="status" id="status">  
          <option value="">All</option> 
          <option value="0">Released</option> 
          <option value="1">Canceled</option> 
        </select>    
      </li>   
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-danger">
          <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
            <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
            <button onClick="history.back()" type="button" class="btn btn-primary btn-flat" data-toggle="tooltip" data-placement="top" title="Back to previous page!"> <i class="fa fa-undo"></i> Back</button>
            <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
            <button class="btn btn-warning btn-flat" onclick="bulk_cancel()"><i class="glyphicon glyphicon-minus"></i> Bulk Cancel</button>
            <div class="box-tools pull-right">
              <div class="tableTools-container">
              </div>
            </div><!-- /.box-tools -->
          </div>

          <div class="box-header">
            <!-- /.panel-heading -->
            <div class="box-body">
              <table id="datagrid" class="table table-bordered table-hover stripe">
                <thead>
                  <tr>
                    <th style="width:5%">
                      <label class="control control--checkbox">
                        <input type="checkbox" id="check-all"/>
                        <div class="control__indicator"></div>
                      </label>
                    </th>
                    <th>No Trans</th>
                    <th>Date Trans</th>
                    <th>Prepaid Name</th>
                    <th>Account</th>
                    <th>Cost</th>
                    <th>Useful Life</th>
                    <th>Begin Amount</th>
                    <th>Remark</th>
                    <th>Status</th> 
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Popup -->
    <div class="modal fade MyModals"  id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
     <div class="modal-dialog" style="width:420px !important;">
      <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select the transaction code</h4>
      </div>
      <form class="form-horizontal" id="formData" name="formaccount"  method="post" action="" enctype="multipart/form-data">
        <div class="modal-body">
         <input type="hidden" class="form-control" id="idperiod" name="idperiod">
         <input type="hidden" class="form-control" id="type" name="type">
         <div class="form-group">
          <label for="real_name" class="col-sm-3 control-label">Transaction</label>
          <div class="col-sm-8">
            <select class="form-control"  style="width: 100%;" name="codetrans" id="codetrans"  placeholder="Transaction Code">
              <option value="PREX">Prepaid Expense</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning btn-flat"><i class="fa fa-plus"></i> Submit</button>
        <input type="hidden" value="1" name="submit" />
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      </div>
    </form>
  </div>

</section>
</div>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {

        //datatables
        table = $('#datagrid').DataTable({ 

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "responsive": true,
            "pageLength": 50,
            "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
            "scrollY": "440px",
            // Load data for the table's content from an Ajax source
            "ajax": {
              "url": "<?php echo site_url('prepaid/ajax_list')?>",
              "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
              },
              { 
                "targets": [ 0 ], //first column
                "orderable": false, //set not orderable
              },
              { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
              }
              ],

            });

        //datepicker
        $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          todayHighlight: true,
          orientation: "top auto",
          todayBtn: true,
          todayHighlight: true,  
        });

        //set input/textarea/select event when change value, remove class error and remove text help block 
        $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
        });
        $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
        });
        $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
        });

        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });

      });
    
    function add()
    {
      save_method = 'add';
        //$('#form')[0].reset(); // reset form on modals
        //$('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        //$('.modal-title').text('Select a slip'); // Set Title to Bootstrap modal title
      }

      function edit(idprepaid)
      {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : "<?php echo site_url('prepaid/ajax_edit/')?>/" + idprepaid,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="idprepaid"]').val(data.idprepaid);
            $('[name="prepaidname"]').val(data.prepaidname);
            $('[name="description"]').val(data.description);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Currency'); // Set title to Bootstrap modal title

              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function delete_id(idprepaid)
      {
        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete this data?',
          type: 'orange',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () {
                    //$.alert('Canceled!');
                  }
                },
                confirm: {
                  text: 'CANCEL',
                  btnClass: 'btn-orange',
                  action: function () {
                   $.ajax({
                    url : "<?php echo site_url('prepaid/ajax_delete')?>/"+idprepaid,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $.alert({
                      type: 'red',
                        icon: 'fa fa-orange', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error canceling data!',
                      });
                  }
                });
                 }
               },

             }
           });
      }

      function bulk_cancel()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to cancel '+list_id.length+' transaction?',
            type: 'orange',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-orange',
              action: function () {
               $.ajax({
                data: {idprepaid:list_id},
                url: "<?php echo site_url('prepaid/ajax_bulk_cancel')?>",
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {
                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'orange',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error deleteing data!',
                              });

                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script>






