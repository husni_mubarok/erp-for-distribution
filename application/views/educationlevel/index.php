
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
      <h4>
        Education Level
    </h4>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Education Level</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
          <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
              <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
              <button onClick="history.back()" type="button" class="btn btn-primary btn-flat" data-toggle="tooltip" data-placement="top" title="Back to previous page!"> <i class="fa fa-undo"></i> Back</button>
              <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
              <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button>
              <div class="box-tools pull-right">
                  <div class="tableTools-container">
                  </div>
              </div><!-- /.box-tools -->
          </div>

          <div class="box-header">
              <!-- /.panel-heading -->
              <div class="box-body">
                <table id="datagrid" class="table table-bordered table-hover stripe">
                    <thead>
                        <tr>
                            <th style="width:5%">
                                <label class="control control--checkbox">
                                  <input type="checkbox" id="check-all"/>
                                  <div class="control__indicator"></div>
                              </label>

                          </th>
                          <th style="width:125px;">Action</th>
                          <th>Education Level</th>
                          <th>Status</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
</div>
</section>
</div>


<script type="text/javascript">
    var save_method; //for save method string
    var table;
    
    console.log("test");

    $(document).ready(function() {

        console.log("test");
        //datatables
        table = $('#datagrid').DataTable({ 

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "responsive": true,
            "pageLength": 50,
            "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
            "scrollY": "360px",
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('educationlevel/ajax_list')?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            { 
                "targets": [ 0 ], //first column
                "orderable": false, //set not orderable
            },
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            }
            ],

        });

        //datepicker
        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            orientation: "top auto",
            todayBtn: true,
            todayHighlight: true,  
        });

        //set input/textarea/select event when change value, remove class error and remove text help block 
        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });

    });
    
    function add()
    {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Education Level'); // Set Title to Bootstrap modal title
    }
    
    function edit(ideducationlevel)
    {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('educationlevel/ajax_edit/')?>/" + ideducationlevel,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                $('[name="ideducationlevel"]').val(data.ideducationlevel);
                $('[name="educationlevelname"]').val(data.educationlevelname);
                $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Expedition Type'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
    
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }
    
    function save()
    {
        //$('#btnSave').text('saving...'); //change button text
        //$('#btnSave').attr('disabled',true); //set button disable 
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('educationlevel/ajax_add')?>";
        } else {
            url = "<?php echo site_url('educationlevel/ajax_update')?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                //$('#btnSave').text('save'); //change button text
                //$('#btnSave').attr('disabled',false); //set button enable 


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.alert({
                type: 'red',
                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                    title: 'Warning',
                    content: 'Error adding or update data!',
                });
                //$('#btnSave').text('save'); //change button text
                //$('#btnSave').attr('disabled',false); //set button enable 

            }
        });
    }
    
    function saveadd()
    {
        //$('#btnSaveAdd').text('Saving...'); //change button text
        //$('#btnSaveAdd').attr('disabled',true); //set button disable 
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('educationlevel/ajax_add')?>";
        } else {
            url = "<?php echo site_url('educationlevel/ajax_update')?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    //$('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                //$('#btnSaveAdd').text('Save and Add'); //change button text
                //$('#btnSaveAdd').attr('disabled',false); //set button enable 
                //add_customer();

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.alert({
                type: 'red',
                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                    title: 'Warning',
                    content: 'Error adding or update data!',
                });
                //$('#btnSaveAdd').text('Save and Add'); //change button text
                //$('#btnSaveAdd').attr('disabled',false); //set button enable 
                //add_customer();

            }
        });
    }
    
    function delete_id(ideducationlevel)
    {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete this data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                   action: function () {
                    //$.alert('Canceled!');
                }
            },
            confirm: {
                text: 'DELETE',
                btnClass: 'btn-red',
                action: function () {
                   $.ajax({
                    url : "<?php echo site_url('educationlevel/ajax_delete')?>/"+ideducationlevel,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $.alert({
                    type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                    });
                }
            });
               }
           },

       }
   });
    }

    function bulk_delete()
    {


        var list_id = [];
        $(".data-check:checked").each(function() {
            list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure to delete '+list_id.length+' data?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    cancel: {
                       action: function () {

                       }
                   },
                   confirm: {
                    text: 'DELETE',
                    btnClass: 'btn-red',
                    action: function () {
                       $.ajax({
                        data: {ideducationlevel:list_id},
                        url: "<?php echo site_url('educationlevel/ajax_bulk_delete')?>",
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                            if(data.status)
                            {
                                reload_table();
                            }
                            else
                            {
                                alert('Failed.');
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            $.alert({
                            type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error deleteing data!',
                            });

                        }
                    });
                   }
               },

           }
       });
        }
        else
        {
           $.alert({
            type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
            });
       }
   }
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width:40% !important">
        <div class="modal-content">
            <form action="#" id="form" class="form-horizontal">
                <div class="modal-header" style="height: 60px">
                    <div class="form-group pull-right">
                        <label for="real_name" class="col-sm-4 control-label">Status</label>
                        <div class="col-sm-8 pull-right">
                            <select class="form-control" style="width: 100%;" name="status"  id="status">
                             <option value="0">Active</option>
                             <option value="1">Not Active</option>
                         </select>
                     </div>
                 </div>
                 <h3 class="modal-title">Education Level Form</h3>
             </div>
             <div class="modal-body">

             <input type="hidden" value="" name="ideducationlevel"/> 
                <div class="form-body">
                    <div class="row">

                        <div class="form-group">
                            <label class="control-label col-md-3">Education Level</label>
                            <div class="col-md-8">
                                <input name="educationlevelname" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
                <button type="button" id="btnSave" onClick="save()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



