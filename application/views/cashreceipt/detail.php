<?php 
$segment = $this->uri->segment('3');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>
      Cash Receipt
    </h4>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Transaction</a></li>
      <li class="active">Cash Receipt</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
     <div class="col-xs-12">
      <div class="box box-danger">
        <form class="form-horizontal" id="formData" name="formData"  method="post" action="" enctype="multipart/form-data">
         <!--  Hidden element -->
         <input type="hidden" value="<?php echo $detail['idtrans'];?>" id="idtrans" name="idtrans">
         <div class="box-header with-border">
           <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-4">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $detail['notrans'];?>" class="form-control" id="transno" name="transno" disabled="disabled">
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                  <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                   <input type="text" class="form-control pull-right" id="datetrans" name="datetrans" data-toggle="tooltip" data-placement="transdate" data-date-format="dd/mm/yyyy" value="<?php echo $detail['datetrans'];?>" onchange="saveprint();">
                 </div><!-- /.input group -->
               </div>
             </div>
             <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Customer</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="customerid" id="customerid" onchange="saveprint();">
                  <option value="<?php echo $detail['customerid'];?>"><?php echo $detail['customername'];?></option>
                  <?php foreach($customer as $customers):?>
                    <option value="<?php echo $customers->idcustomer;?>"><?php echo $customers->customername;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
          </div>
          <!-- Coloumn 2-->  

          <div class="col-md-4">
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Payment Type</label>
              <div class="col-sm-9">
                <select class="form-control select2"  style="width: 100%;" name="paytypeid" id="paytypeid"  placeholder="Payment Type" onchange="saveprint();">
                  <option value="<?php echo $detail['paytypeid'];?>"><?php echo $detail['paytypename'];?></option>
                  <?php foreach($paytype as $paytypes):?>
                    <option value="<?php echo $paytypes->idpaytype;?>"><?php echo $paytypes->paytypename;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Cash Account</label>
              <div class="col-sm-9">
                <select class="form-control select2"  style="width: 100%;" name="accountcashid" id="accountcashid"  placeholder="Cash Account" onchange="saveprint();">
                  <option value="<?php echo $detail['accountcashid'];?>"><?php echo $detail['accountname'];?></option>
                  <?php foreach($cashaccount as $cashaccounts):?>
                    <option value="<?php echo $cashaccounts->idaccount;?>"><?php echo $cashaccounts->accountname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Currency</label>
              <div class="col-sm-9">
                <select class="form-control select2" name="currencyid" onchange="saveprint();">
                  <option value="<?php echo $detail['currencyid'];?>"><?php echo $detail['currencyname'];?></option>
                  <?php foreach($currency as $currencys):?>
                    <option value="<?php echo $currencys->idcurrency;?>"><?php echo $currencys->currencyname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
          </div>
          <!-- Coloumn 3-->                                   
          <div class="col-md-4">
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Giro No</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo $detail['girono'];?>" class="form-control" id="girono" name="girono" onchange="saveprint();">
              </div>
            </div>
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Giro Date</label>
              <div class="col-sm-9">
                <div class="input-group">
                 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                 <input type="text" class="form-control pull-right" id="girodate" name="girodate" data-toggle="tooltip" data-placement="girodate" data-date-format="dd/mm/yyyy" value="<?php echo $detail['girodate'];?>" onchange="saveprint();">
               </div><!-- /.input group -->
             </div>
           </div>

           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Remark</label>
            <div class="col-sm-9">
              <textarea class="form-control" id="remark" name="remark" onchange="saveprint();"><?php echo $detail['remark'];?></textarea>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <table id="dg" style="width:100%;height:330px"
      toolbar="#toolbar" pagination="true" idField="idtransdet"
      rownumbers="true" fitColumns="true" showFooter="true" data-options="pagination:true, rownumbers:true, autoRowHeight:false, pageSize:20, singleSelect:false">
      <thead>
        <tr>
          <th field="idtransdet" checkbox="true"></th>
          <th data-options="field:'accountid', width:'30', sortable:true, nowrap:true,
          formatter:function(value,row){
          return row.accountcode;
        },
        editor:{
        type:'combogrid',
        options:{
        panelWidth:530,
        pagination: true,
        mode:'remote',
        idField:'idaccount',
        textField:'accountcode',
        url:'<?php echo site_url('combogrid/account'); ?>',
        columns:[[
        {field:'accountcode',title:'Account Code',width:80,sortable:true},
        {field:'accountname',title:'Account Name',width:200,sortable:true} 
        ]],
        onSelect: function(index,row){
        setTimeout(function(){ 
        var opts = $('#dg').edatagrid('options');
        var edaccount = $('#dg').edatagrid('getEditor',{
        index:opts.editIndex,
        field:'accountname'
      });   
      $(edaccount.target).combobox('setValue',row.accountname);
    },0);
  },
  fitColumns: true
}
}"><b>Account Code</b></th>  
<th data-options="field:'accountname',width:50, sortable:true, 
formatter:function(value,row){
return row.accountname;
},
editor:{
type:'combobox'
}"><b>Account Name</b></th>
<th data-options="field:'costitemid', width:'50', sortable:true, nowrap:true,
formatter:function(value,row){
return row.employeename;
},
editor:{
type:'combogrid',
options:{
panelWidth:530,
pagination: true,
mode:'remote',
idField:'idemployee',
textField:'employeename',
url:'<?php echo site_url('combogrid/employee'); ?>',
columns:[[
{field:'nik',title:'NIK',width:80,sortable:true},
{field:'employeename',title:'Employee Name',width:200,sortable:true},
{field:'positionname',title:'Position',width:200,sortable:true} ,
{field:'departmentname',title:'Department',width:200,sortable:true}  
]],
onSelect: function(index,row){
setTimeout(function(){   
},0);
},
fitColumns: true
}
}"><b>Cost Item</b></th>
<th data-options="field:'costcenterid',width:40, sortable:true, 
formatter:function(value,row){
return row.costcentername;
},
editor:{
type:'combobox',
options:{
valueField:'idcostcenter',
textField:'costcentername',
method:'get',
url:'<?php echo site_url('combobox/costcenter'); ?>',
required:true
}
}"><b>Cost Center</b></th>
<th field="debt" sortable="true" width="25" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}"><b>Debt</b></th>
<th field="credit" sortable="true" width="25" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}"><b>Credit</b></th>
<th field="amount" sortable="true" width="25" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}"><b>Amount</b></th>
</tr>
</thead>
</table>

<div id="toolbar">
  <table cellpadding="0" cellspacing="0" style="width:100%">
    <tr>
     <td>
      <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dg').edatagrid('addRow')">New</a> |
      <a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')">Accept</a> |
      <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dg').edatagrid('destroyRow')">Destroy</a> |
      <a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dg').edatagrid('cancelRow')">Cancel</a> |
      <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="refreshgrid()">Refresh</a>
    </td>
    <td style="text-align:right">
      <input type="text" placeholder=" Search..." id="msearchdetail" style="line-height:18px;border:1px solid #ccc" onkeydown="keyCode(event)">
      <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()"></a>
    </td>
  </tr>
</table>
</div>    
</div><!-- /.col -->
</div>
</div>
</div>
<div class="box-footer" style="margin-top:-40px !important">
 <?php
  if($detail['status']==0) {
    $btnstatus = 'Released';
    $btnclass = 'fa fa-arrow-right';
  }elseif ($detail['status']==9) {
    $btnstatus = 'Reopen';
    $btnclass = 'fa fa-arrow-left';
  }else{
    $btnstatus = 'Open';
    $btnclass = 'fa fa-arrow-left';
  };
  ?>
<button class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow')"><i class="<?php echo $btnclass; ?>"></i> <?php echo $btnstatus; ?></button> 
  <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveprint();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a>
  <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a>
  <a href="<?php echo site_url('cashreceipt');?>" type="button" class="btn btn-default btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
  <iframe src='<?php echo site_url('cashreceipt/detail/8');?>"' height="0" width="0" frameborder='0' name="print_frame"></iframe>
</div>
</form>
</div>
</section><!-- /.content -->
</div>
</div>
</section>

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid">
          <option ondblclick="window.open('',target='_blank')" value="cashreceipt">Cash Receipt</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-flat" onclick="showslip()"> <i class="fa fa-search"></i> Preview</button>
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- /.javascript -->
<script type="text/javascript">
  $(function(){
    $('#dg').edatagrid({
      url: '<?php echo site_url('cashreceipt/list_detail/'.$segment.''); ?>?grid=true',
      saveUrl: '<?php echo site_url('cashreceipt/create_detail/'.$segment.''); ?>',
      updateUrl: '<?php echo site_url('cashreceipt/update_detail'); ?>',
      destroyUrl: '<?php echo site_url('cashreceipt/delete_detail'); ?>',

      onBeforeSave: function(index){
        var account = $(this).edatagrid('getEditor', {
          index: index,
          field: 'accountid'
        });

        var costitem = $(this).edatagrid('getEditor', {
          index: index,
          field: 'costitemid'
        });

        var costcenter = $(this).edatagrid('getEditor', {
          index: index,
          field: 'costcenterid'
        });

        var row = $(this).edatagrid('getRows')[index];
        row.accountcode = $(account.target).combobox('getText');
        row.costitemname = $(costitem.target).combobox('getText');
        row.costcentername = $(costcenter.target).combobox('getText');
      }

    });
  });

// Calculate two coloumn or more
$(function(){
  var lastIndex;
  $('#dg').datagrid({
    onBeginEdit:function(rowIndex){
      var editors = $('#dg').datagrid('getEditors', rowIndex);
      var n1 = $(editors[4].target);
      var n2 = $(editors[5].target); 
      var n3 = $(editors[6].target); 
      n1.add(n2).numberbox({
        onChange:function(){
          var cost2 = (n1.numberbox('getValue')-n2.numberbox('getValue'));
          n3.numberbox('setValue',cost2);
        }
      })
    }
  });
});

function doSearch(){
  $('#dg').datagrid('load',{
    msearchdetail: $('#msearchdetail').val(),
  });
}

function keyCode(event) {
  var x = event.keyCode;
  if (x == 13) {
    doSearch();
  }
  return false;
}
$('#formData').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

function refreshgrid()
{
  $('#dg').datagrid('reload'); 
}
function expandAllRow()
{
  var dg = $('#dg');
  var count = dg.datagrid('getRows').length;
  for(var i=0; i<count; i++){
    dg.datagrid('expandRow',i);
  }
}
function showslip()
{
 var slipid = document.getElementById("slipid").value;
 var url = "report.php?mod=" + slipid + "&key="+ key;
 PopupCenterDual(url,'Popup_Window','1000','650');
}

function saveprint()
{
  var url;
  url = "<?php echo site_url('cashreceipt/saveprint')?>";

    // ajax adding data to database
    $.ajax({
      url : url,
      type: "POST",
      data: $('#formData').serialize(),
      dataType: "JSON",
      success: function(data)
      {
             // alert("success!");
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            $.alert({
              type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error adding or update data!',
              });
          }
        });
  }

  function canceltransaction()
  {
    var url;
    url = "<?php echo site_url('cashreceipt/canceltransaction')?>";

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this transaction <b><?php echo $detail['notrans'];?> </b>?',
      type: 'orange',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () {

         }
       },
       confirm: {   
        text: 'CANCEL',
        btnClass: 'btn-orange',
        action: function () {

          $.ajax({
            url : url,
            type: "POST",
            data: $('#formData').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                 // alert("success!");
               },
               error: function (jqXHR, textStatus, errorThrown)
               {
                $.alert({
                  type: 'orange',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error canceling data!',
                              });

              }
            });
        }
      },

    }
  });
  }

</script>
