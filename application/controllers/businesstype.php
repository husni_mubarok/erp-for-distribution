<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class businesstype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('businesstype_model','businesstype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('businesstype/index');
	}

	public function ajax_list()
	{
		$list = $this->businesstype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $businesstype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$businesstype->idbusinesstype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$businesstype->idbusinesstype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$businesstype->idbusinesstype."'".')"> | Delete</a>';
			$row[] = $businesstype->businesstypename;
			if ($businesstype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->businesstype->count_all(),
						"recordsFiltered" => $this->businesstype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idbusinesstype)
	{
		$data = $this->businesstype->get_by_id($idbusinesstype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'businesstypename' => $this->input->post('businesstypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->businesstype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'businesstypename' => $this->input->post('businesstypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->businesstype->update(array('idbusinesstype' => $this->input->post('idbusinesstype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idbusinesstype)
	{
		$this->businesstype->delete_by_id($idbusinesstype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idbusinesstype');
		foreach ($list_id as $idbusinesstype) {
			$this->businesstype->delete_by_id($idbusinesstype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('businesstypename') == '')
		{
			$data['inputerror'][] = 'businesstypename';
			$data['error_string'][] = 'Bank name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
