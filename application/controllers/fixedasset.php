<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class fixedasset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('fixedasset_model','fixedasset');
		$this->load->model('supplier_model', 'supplier'); 
		$this->load->model('currency_model', 'currency'); 
		$this->load->model('account_model', 'account');
		$this->load->model('unit_model', 'unit');
		$this->load->model('depreciationtype_model', 'depreciationtype');
		$this->load->model('inventory_model', 'inventory');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('fixedasset/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO fixedasset (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(fixedasset.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			fixedasset
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('fixedasset/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->fixedasset->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fixedasset) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$fixedasset->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="fixedasset/detail/'.$fixedasset->idtrans.'" title="Edit")"> '.$fixedasset->notrans.'</a>';
			$row[] = $fixedasset->datetrans;
			$row[] = $fixedasset->inventoryname;
			$row[] = $fixedasset->depreciationtypename;
			$row[] = $fixedasset->accountname;
			$row[] = $fixedasset->unit;   

			if ($fixedasset->status == 0) {
				$row[] = '<span class="label label-success"> Released </span>';
			}else{
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->fixedasset->count_all(),
			"recordsFiltered" => $this->fixedasset->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('inventoryid','Currency','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
							fixedasset.idtrans,
							fixedasset.codetrans,
							fixedasset.notrans,
							DATE_FORMAT(
								fixedasset.datetrans,
								'%d/%m/%Y'
							) AS datetrans,
							fixedasset.currencyid,
							currency.currencyname,
							fixedasset.remark,
							fixedasset.`status`,
							fixedasset.depreciationtypeid,
							depreciationtype.depreciationtypename,
							fixedasset.inventoryid,
							inventory.inventoryname,
							fixedasset.spesification,
							fixedasset.dateacquisition,
							fixedasset.accountid,
							account.accountname,
							fixedasset.quantity,
							fixedasset.unit,
							fixedasset.cost,
							fixedasset.degindepreciation,
							fixedasset.salvagevalue,
							fixedasset.usefullife,
							fixedasset.datedisposal,
							CASE WHEN fixedasset.status=0 THEN 1 ELSE 0 END AS statussw,
							fixedasset.status
						FROM
							fixedasset
						LEFT JOIN currency ON fixedasset.currencyid = currency.idcurrency
						LEFT JOIN depreciationtype ON fixedasset.depreciationtypeid = depreciationtype.iddepreciationtype
						LEFT JOIN inventory ON fixedasset.inventoryid = inventory.idinventory
						LEFT JOIN account ON fixedasset.accountid = account.idaccount
						WHERE fixedasset.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();  
			$data['supplier'] = $this->supplier->getdata()->result();  
			$data['unit'] = $this->unit->getdata()->result(); 
			$data['account'] = $this->account->getdata()->result();
			$data['depreciationtype'] = $this->depreciationtype->getdata()->result();  
			$data['inventory'] = $this->inventory->getdata()->result(); 

			$this->template->display('fixedasset/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date)); 

			$varreversedate = $this->input->post('reversedate', true); 
			$datereversedate = str_replace('/', '-', $varreversedate);
			$reversedate = date('Y-m-d', strtotime($datereversedate)); 

			$this->db->where('idtrans', $id);
			$this->db->update('fixedasset',array(
				'datetrans'=>$datetrans,  
				'supplierid'=>$this->input->post('supplierid', true), 
				'inventoryid'=>$this->input->post('inventoryid', true),
				'accountid'=>$this->input->post('accountid', true),
				'depreciationtypeid'=>$this->input->post('depreciationtypeid', true),
				'usefullife'=>$this->input->post('usefullife', true),
				'quantity'=>$this->input->post('quantity', true), 
				'unit'=>$this->input->post('unit', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('fixedasset');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));  

		$varreversedate = $this->input->post('reversedate', true); 
		$datereversedate = str_replace('/', '-', $varreversedate);
		$reversedate = date('Y-m-d', strtotime($datereversedate)); 

		//$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,  
				'supplierid'=>$this->input->post('supplierid', true), 
				'inventoryid'=>$this->input->post('inventoryid', true),
				'accountid'=>$this->input->post('accountid', true),
				'depreciationtypeid'=>$this->input->post('depreciationtypeid', true),
				'usefullife'=>$this->input->post('usefullife', true),
				'quantity'=>$this->input->post('quantity', true), 
				'unit'=>$this->input->post('unit', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->fixedasset->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->fixedasset->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->fixedasset->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('supplierid') == '')
		{
			$data['inputerror'][] = 'supplierid';
			$data['error_string'][] = 'Supplier is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(account.accountname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		fixedassetdet
					LEFT JOIN account ON fixedassetdet.accountdepreciationid = account.idaccount
					LEFT JOIN costcenter ON fixedassetdet.costcenterid = costcenter.idcostcenter where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
					fixedassetdet.idtransdet,
					fixedassetdet.transid,
					fixedassetdet.accountdepreciationid,
					account.accountcode,
					account.accountname,
					fixedassetdet.costcenterid,
					costcenter.costcentername,  
					FORMAT(fixedassetdet.amount,0) AS amount 
					FROM
					fixedassetdet
					LEFT JOIN account ON fixedassetdet.accountdepreciationid = account.idaccount
					LEFT JOIN costcenter ON fixedassetdet.costcenterid = costcenter.idcostcenter 
					WHERE fixedassetdet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'accountdepreciationid'=>$data['accountdepreciationid'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'],
				'costcenterid'=>$data['costcenterid'],
				'costcentername'=>$data['costcentername'], 
				'amount'=>$data['amount']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(amount),0) AS amount FROM
		fixedassetdet
		LEFT JOIN account ON fixedassetdet.accountdepreciationid = account.idaccount
		LEFT JOIN costcenter ON fixedassetdet.costcenterid = costcenter.idcostcenter 
		WHERE fixedassetdet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('description'=>'Total',
				'debt'=>$data['debt'],
				'credit'=>$data['credit']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->fixedasset->create_detail($segment)) 
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->fixedasset->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->fixedasset->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
