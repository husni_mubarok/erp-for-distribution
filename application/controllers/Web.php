<?php
class Web extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model(array('user_model'));
         if($this->session->userdata('username')){
            redirect('dashboard');
         }
    }
    
    function index(){
        $this->load->view('web/login');
    }
    
    function process(){

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Username','required|trim|xss_clean');
        $this->form_validation->set_rules('password','password','required|trim|xss_clean');
        
        if($this->form_validation->run()==false){
            $this->session->set_flashdata('message','Username or password is not valid');
            redirect('web');
        }else{
            $username=$this->input->post('username');
            $password=$this->input->post('password');
            $cek=$this->user_model->cek($username,md5($password));
            if($cek->num_rows()>0){
                //login success, create session
                $this->session->set_userdata('username',$username);
                redirect('dashboard');
                
            }else{
                //login faild
                $this->session->set_flashdata('message','Username or password is not valid');
                redirect('web');
            }
        }
    }
}