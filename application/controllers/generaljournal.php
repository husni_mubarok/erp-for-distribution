<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class generaljournal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('generaljournal_model','generaljournal'); 
		$this->load->model('currency_model', 'currency');  
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('generaljournal/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO generaljournal (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(generaljournal.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			generaljournal
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('generaljournal/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->generaljournal->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $generaljournal) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$generaljournal->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="generaljournal/detail/'.$generaljournal->idtrans.'" title="Edit")"> '.$generaljournal->notrans.'</a>';
			$row[] = $generaljournal->datetrans;
			$row[] = $generaljournal->noref;
			$row[] = $generaljournal->currencyname;
			$row[] = $generaljournal->reverse;
			$row[] = $generaljournal->reversedate;  
			$row[] = $generaljournal->remark;

			if ($salesorder->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($salesorder->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($salesorder->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->generaljournal->count_all(),
			"recordsFiltered" => $this->generaljournal->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('currencyid','Currency','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
						generaljournal.idtrans,
						generaljournal.codetrans,
						generaljournal.notrans,
						DATE_FORMAT(generaljournal.datetrans,'%d/%m/%Y') AS datetrans,
						generaljournal.noref,
						generaljournal.currencyid,
						currency.currencyname,
						generaljournal.reverse, 
						DATE_FORMAT(generaljournal.reversedate,'%d/%m/%Y') AS reversedate,
						generaljournal.remark,
						CASE WHEN generaljournal.status=0 THEN 1 ELSE 0 END AS statussw,
						generaljournal.status
						FROM
						generaljournal
						LEFT JOIN currency ON generaljournal.currencyid = currency.idcurrency
			WHERE generaljournal.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array(); 
			$data['currency'] = $this->currency->getdata()->result(); 

			$this->template->display('generaljournal/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date)); 

			$varreversedate = $this->input->post('reversedate', true); 
			$datereversedate = str_replace('/', '-', $varreversedate);
			$reversedate = date('Y-m-d', strtotime($datereversedate)); 

			$this->db->where('idtrans', $id);
			$this->db->update('generaljournal',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('generaljournal');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));  

		$varreversedate = $this->input->post('reversedate', true); 
		$datereversedate = str_replace('/', '-', $varreversedate);
		$reversedate = date('Y-m-d', strtotime($datereversedate)); 

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans, 
				'reversedate'=>$reversedate, 
				'currencyid'=>$this->input->post('currencyid', true), 
				'noref'=>$this->input->post('noref', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->generaljournal->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->generaljournal->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->generaljournal->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('currencyid') == '')
		{
			$data['inputerror'][] = 'currencyid';
			$data['error_string'][] = 'Currency is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(account.accountname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		generaljournaldet
					LEFT JOIN account ON generaljournaldet.accountid = account.idaccount
					LEFT JOIN costcenter ON generaljournaldet.costcenterid = costcenter.idcostcenter where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
					generaljournaldet.idtransdet,
					generaljournaldet.transid,
					generaljournaldet.accountid,
					account.accountcode,
					account.accountname,
					generaljournaldet.costcenterid,
					costcenter.costcentername,  
					FORMAT(generaljournaldet.debt,0) AS debt,
					FORMAT(generaljournaldet.credit,0) AS credit 
					FROM
					generaljournaldet
					LEFT JOIN account ON generaljournaldet.accountid = account.idaccount
					LEFT JOIN costcenter ON generaljournaldet.costcenterid = costcenter.idcostcenter 
					WHERE generaljournaldet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'accountid'=>$data['accountid'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'],
				'costcenterid'=>$data['costcenterid'],
				'costcentername'=>$data['costcentername'], 
				'debt'=>$data['debt'],
				'credit'=>$data['credit'] 
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(debt),0) AS debt, FORMAT(SUM(credit),0) AS credit FROM
		generaljournaldet
		LEFT JOIN account ON generaljournaldet.accountid = account.idaccount
		LEFT JOIN costcenter ON generaljournaldet.costcenterid = costcenter.idcostcenter 
		WHERE generaljournaldet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('description'=>'Total',
				'debt'=>$data['debt'],
				'credit'=>$data['credit']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->generaljournal->create_detail($segment)) 
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->generaljournal->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->generaljournal->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
