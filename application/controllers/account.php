<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('account_model','account');
		$this->load->model('accountlvl1_model','accountlvl1');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$data['accountlvl1']=$this->accountlvl1->getdata()->result();
		
		$this->template->display('account/index', $data);
		
	}

	public function ajax_list()
	{
		$list = $this->account->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $account) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$account->idaccount.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$account->idaccount."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$account->idaccount."'".')"> | Delete</a>';
			$row[] = $account->accountcode;
			$row[] = $account->accountname;
			$row[] = $account->accountlvl1name;

			if ($account->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->account->count_all(),
						"recordsFiltered" => $this->account->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idaccount)
	{
		$data = $this->account->get_by_id($idaccount);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'accountcode' => $this->input->post('accountcode'),
				'accountname' => $this->input->post('accountname'),
				'accountlvl1id' => $this->input->post('accountlvl1id'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->account->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'accountcode' => $this->input->post('accountcode'),
				'accountname' => $this->input->post('accountname'),
				'accountlvl1id' => $this->input->post('accountlvl1id'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->account->update(array('idaccount' => $this->input->post('idaccount')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idaccount)
	{
		$this->account->delete_by_id($idaccount);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idaccount');
		foreach ($list_id as $idaccount) {
			$this->account->delete_by_id($idaccount);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('accountname') == '')
		{
			$data['inputerror'][] = 'accountname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
