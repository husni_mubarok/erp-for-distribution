<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customertype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('customertype_model','customertype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('customertype/index');
	}

	public function ajax_list()
	{
		$list = $this->customertype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customertype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$customertype->idcustomertype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$customertype->idcustomertype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$customertype->idcustomertype."'".')"> | Delete</a>';
			$row[] = $customertype->customertypename;

			if ($customertype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->customertype->count_all(),
						"recordsFiltered" => $this->customertype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcustomertype)
	{
		$data = $this->customertype->get_by_id($idcustomertype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'customertypename' => $this->input->post('customertypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->customertype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'customertypename' => $this->input->post('customertypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$this->customertype->update(array('idcustomertype' => $this->input->post('idcustomertype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcustomertype)
	{
		$this->customertype->delete_by_id($idcustomertype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcustomertype');
		foreach ($list_id as $idcustomertype) {
			$this->customertype->delete_by_id($idcustomertype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customertypename') == '')
		{
			$data['inputerror'][] = 'customertypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
