<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventorygroup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventorygroup_model','inventorygroup');
		
		 $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('inventorygroup/index');
	}

	public function ajax_list()
	{
		$list = $this->inventorygroup->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventorygroup) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventorygroup->idinventorygroup.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventorygroup->idinventorygroup."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventorygroup->idinventorygroup."'".')"> | Delete</a>';
			$row[] = $inventorygroup->inventorygroupname;

			if ($inventorygroup->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventorygroup->count_all(),
						"recordsFiltered" => $this->inventorygroup->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventorygroup)
	{
		$data = $this->inventorygroup->get_by_id($idinventorygroup);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorygroupname' => $this->input->post('inventorygroupname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->inventorygroup->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorygroupname' => $this->input->post('inventorygroupname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->inventorygroup->update(array('idinventorygroup' => $this->input->post('idinventorygroup')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventorygroup)
	{
		$this->inventorygroup->delete_by_id($idinventorygroup);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventorygroup');
		foreach ($list_id as $idinventorygroup) {
			$this->inventorygroup->delete_by_id($idinventorygroup);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventorygroupname') == '')
		{
			$data['inputerror'][] = 'inventorygroupname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
