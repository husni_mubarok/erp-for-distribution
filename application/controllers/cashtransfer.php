<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cashtransfer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('account_model','account'); 
		$this->load->model('cashtransfer_model', 'cashtransfer');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('cashtransfer/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO cashtransfer (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(cashtransfer.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			cashtransfer
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('cashtransfer/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->cashtransfer->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cashtransfer) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$cashtransfer->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="cashtransfer/detail/'.$cashtransfer->idtrans.'" title="Edit")"> '.$cashtransfer->notrans.'</a>';
			$row[] = $cashtransfer->datetrans;
			$row[] = $cashtransfer->accountfrom;
			$row[] = $cashtransfer->accountto;
			$row[] = $cashtransfer->amount;
			$row[] = $cashtransfer->rate;
			$row[] = $cashtransfer->charge; 
			$row[] = $cashtransfer->remark;

			if ($cashtransfer->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($cashtransfer->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($cashtransfer->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cashtransfer->count_all(),
			"recordsFiltered" => $this->cashtransfer->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('acccashfromid','Account','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
							cashtransfer.idtrans,
							cashtransfer.codetrans,
							cashtransfer.notrans,
							DATE_FORMAT(
								cashtransfer.datetrans,
								'%d/%m/%Y'
							) AS datetrans,
							cashtransfer.acccashfromid,
							account.accountname AS accountfrom,
							cashtransfer.acccashtoid,
							accounto.accountname AS accountto,
							cashtransfer.amount,
							cashtransfer.rate,
							cashtransfer.charge,
							cashtransfer.approveddate,
							cashtransfer.remark,
							cashtransfer.`status`
						FROM
							cashtransfer
						LEFT JOIN account ON cashtransfer.acccashfromid = account.idaccount
						LEFT JOIN account AS accounto ON cashtransfer.acccashtoid = accounto.idaccount
						WHERE cashtransfer.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['account'] = $this->account->getdata()->result(); 

			$this->template->display('cashtransfer/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');
 
			$this->db->where('idtrans', $id);
			$this->db->update('cashtransfer',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('cashtransfer');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$vargirodate = $this->input->post('girodate', true); 
		$date = str_replace('/', '-', $vargirodate);
		$girodate = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'acccashfromid'=>$this->input->post('acccashfromid', true),
				'acccashtoid'=>$this->input->post('acccashtoid', true),
				'amount'=>$this->input->post('amount', true),
				'rate'=>$this->input->post('rate', true),
				'charge'=>$this->input->post('charge', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashtransfer->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashtransfer->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->cashtransfer->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('acccashfromid') == '')
		{
			$data['inputerror'][] = 'acccashfromid';
			$data['error_string'][] = 'Account from is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}
