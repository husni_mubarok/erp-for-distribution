<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class educationlevel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('educationlevel_model','educationlevel');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('educationlevel/index');
	}

	public function ajax_list()
	{
		$list = $this->educationlevel->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $educationlevel) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$educationlevel->ideducationlevel.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$educationlevel->ideducationlevel."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$educationlevel->ideducationlevel."'".')"> | Delete</a>';
			$row[] = $educationlevel->educationlevelname;

			if ($educationlevel->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->educationlevel->count_all(),
						"recordsFiltered" => $this->educationlevel->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($ideducationlevel)
	{
		$data = $this->educationlevel->get_by_id($ideducationlevel);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'educationlevelname' => $this->input->post('educationlevelname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->educationlevel->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'educationlevelname' => $this->input->post('educationlevelname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->educationlevel->update(array('ideducationlevel' => $this->input->post('ideducationlevel')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($ideducationlevel)
	{
		$this->educationlevel->delete_by_id($ideducationlevel);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('ideducationlevel');
		foreach ($list_id as $ideducationlevel) {
			$this->educationlevel->delete_by_id($ideducationlevel);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('educationlevelname') == '')
		{
			$data['inputerror'][] = 'educationlevelname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
