<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('customer_model','customer');
		$this->load->model('bank_model', 'bank');
		$this->load->model('customertype_model', 'customertype');
		$this->load->model('businesstype_model', 'businesstype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$data['bank']=$this->bank->getdata()->result();
		$data['customertype']=$this->customertype->getdata()->result();
		$data['businesstype']=$this->businesstype->getdata()->result();
		//var_dump($data);
		$this->template->display('customer/index', $data);
	}

	public function ajax_list()
	{
		$list = $this->customer->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customer) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$customer->idcustomer.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$customer->idcustomer."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$customer->idcustomer."'".')"> | Delete</a>';
			$row[] = $customer->customercode;
			$row[] = $customer->customername;
			$row[] = $customer->phone1;
			$row[] = $customer->hp1;
			$row[] = $customer->emailaddress;
			$row[] = $customer->website;
			$row[] = $customer->fax;
			$row[] = $customer->reknumber;
			$row[] = $customer->zipcode;

			if ($customer->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->customer->count_all(),
						"recordsFiltered" => $this->customer->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcustomer)
	{
		$data = $this->customer->get_by_id($idcustomer);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'customercode' => $this->input->post('customercode'),
				'customername' => $this->input->post('customername'),
				'phone1' => $this->input->post('phone1'),
				'hp1' => $this->input->post('hp1'),
				'emailaddress' => $this->input->post('emailaddress'),
				'fax' => $this->input->post('fax'),
				'address1' => $this->input->post('address1'),
				'reknumber' => $this->input->post('reknumber'),
				'zipcode' => $this->input->post('zipcode'),
				'customertypeid' => $this->input->post('customertypeid'),
				'bankaccountid' => $this->input->post('bankaccountid'),
				'businesstypeid' => $this->input->post('businesstypeid'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->customer->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'customercode' => $this->input->post('customercode'),
				'customername' => $this->input->post('customername'),
				'phone1' => $this->input->post('phone1'),
				'hp1' => $this->input->post('hp1'),
				'emailaddress' => $this->input->post('emailaddress'),
				'fax' => $this->input->post('fax'),
				'address1' => $this->input->post('address1'),
				'reknumber' => $this->input->post('reknumber'),
				'zipcode' => $this->input->post('zipcode'),
				'customertypeid' => $this->input->post('customertypeid'),
				'bankaccountid' => $this->input->post('bankaccountid'),
				'businesstypeid' => $this->input->post('businesstypeid'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->customer->update(array('idcustomer' => $this->input->post('idcustomer')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcustomer)
	{
		$this->customer->delete_by_id($idcustomer);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcustomer');
		foreach ($list_id as $idcustomer) {
			$this->customer->delete_by_id($idcustomer);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customername') == '')
		{
			$data['inputerror'][] = 'customername';
			$data['error_string'][] = 'Customer name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
