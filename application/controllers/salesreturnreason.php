<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class salesreturnreason extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('salesreturnreason_model','salesreturnreason');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('salesreturnreason/index');
	}

	public function ajax_list()
	{
		$list = $this->salesreturnreason->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $salesreturnreason) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$salesreturnreason->idsalesreturnreason.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$salesreturnreason->idsalesreturnreason."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$salesreturnreason->idsalesreturnreason."'".')"> | Delete</a>';
			$row[] = $salesreturnreason->salesreturnreasonname;

			if ($salesreturnreason->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->salesreturnreason->count_all(),
						"recordsFiltered" => $this->salesreturnreason->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idsalesreturnreason)
	{
		$data = $this->salesreturnreason->get_by_id($idsalesreturnreason);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'salesreturnreasonname' => $this->input->post('salesreturnreasonname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->salesreturnreason->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'salesreturnreasonname' => $this->input->post('salesreturnreasonname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesreturnreason->update(array('idsalesreturnreason' => $this->input->post('idsalesreturnreason')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idsalesreturnreason)
	{
		$this->salesreturnreason->delete_by_id($idsalesreturnreason);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idsalesreturnreason');
		foreach ($list_id as $idsalesreturnreason) {
			$this->salesreturnreason->delete_by_id($idsalesreturnreason);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('salesreturnreasonname') == '')
		{
			$data['inputerror'][] = 'salesreturnreasonname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
