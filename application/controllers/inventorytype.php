<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventorytype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventorytype_model','inventorytype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('inventorytype/index');
	}

	public function ajax_list()
	{
		$list = $this->inventorytype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventorytype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventorytype->idinventorytype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventorytype->idinventorytype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventorytype->idinventorytype."'".')"> | Delete</a>';
			$row[] = $inventorytype->inventorytypename;

			if ($inventorytype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventorytype->count_all(),
						"recordsFiltered" => $this->inventorytype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventorytype)
	{
		$data = $this->inventorytype->get_by_id($idinventorytype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorytypename' => $this->input->post('inventorytypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->inventorytype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorytypename' => $this->input->post('inventorytypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->inventorytype->update(array('idinventorytype' => $this->input->post('idinventorytype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventorytype)
	{
		$this->inventorytype->delete_by_id($idinventorytype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventorytype');
		foreach ($list_id as $idinventorytype) {
			$this->inventorytype->delete_by_id($idinventorytype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventorytypename') == '')
		{
			$data['inputerror'][] = 'inventorytypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
