<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class accountlvl1 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('accountlvl1_model','accountlvl1');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('accountlvl1/index');
	}

	public function ajax_list()
	{
		$list = $this->accountlvl1->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $accountlvl1) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$accountlvl1->idaccountlvl1.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$accountlvl1->idaccountlvl1."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$accountlvl1->idaccountlvl1."'".')"> | Delete</a>';
			$row[] = $accountlvl1->accountlvl1name;

			if ($accountlvl1->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->accountlvl1->count_all(),
						"recordsFiltered" => $this->accountlvl1->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idaccountlvl1)
	{
		$data = $this->accountlvl1->get_by_id($idaccountlvl1);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'accountlvl1name' => $this->input->post('accountlvl1name'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->accountlvl1->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'accountlvl1name' => $this->input->post('accountlvl1name'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->accountlvl1->update(array('idaccountlvl1' => $this->input->post('idaccountlvl1')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idaccountlvl1)
	{
		$this->accountlvl1->delete_by_id($idaccountlvl1);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idaccountlvl1');
		foreach ($list_id as $idaccountlvl1) {
			$this->accountlvl1->delete_by_id($idaccountlvl1);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('accountlvl1name') == '')
		{
			$data['inputerror'][] = 'accountlvl1name';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
