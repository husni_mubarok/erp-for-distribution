<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventorycolor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventorycolor_model','inventorycolor');
		
		 $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('inventorycolor/index');
	}

	public function ajax_list()
	{
		$list = $this->inventorycolor->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventorycolor) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventorycolor->idinventorycolor.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventorycolor->idinventorycolor."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventorycolor->idinventorycolor."'".')"> | Delete</a>';
			$row[] = $inventorycolor->inventorycolorname;

			if ($inventorycolor->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventorycolor->count_all(),
						"recordsFiltered" => $this->inventorycolor->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventorycolor)
	{
		$data = $this->inventorycolor->get_by_id($idinventorycolor);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorycolorname' => $this->input->post('inventorycolorname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->inventorycolor->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorycolorname' => $this->input->post('inventorycolorname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->inventorycolor->update(array('idinventorycolor' => $this->input->post('idinventorycolor')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventorycolor)
	{
		$this->inventorycolor->delete_by_id($idinventorycolor);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventorycolor');
		foreach ($list_id as $idinventorycolor) {
			$this->inventorycolor->delete_by_id($idinventorycolor);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventorycolorname') == '')
		{
			$data['inputerror'][] = 'inventorycolorname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
