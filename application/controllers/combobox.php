<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class combobox extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('combobox_model','combobox');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }   
		
	}

	public function unit()
	{
			echo $this->combobox->getUnit();
	}

	public function rack()
	{
			echo $this->combobox->getRack();
	}

	public function costcenter()
	{
			echo $this->combobox->getCostcenter();
	}
}
