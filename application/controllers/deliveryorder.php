<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class deliveryorder extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->library(array('template','form_validation'));
		$this->load->model('user_model');
		$this->load->model('deliveryorder_model','deliveryorder');
		$this->load->model('customer_model', 'customer');
		$this->load->model('shipto_model','shipto');
		$this->load->model('currency_model', 'currency');
		$this->load->model('salesman_model', 'salesman');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn'); 
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}	
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('deliveryorder/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO deliveryorder (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(deliveryorder.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			deliveryorder
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('deliveryorder/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->deliveryorder->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $deliveryorder) {
			$no++;
			$row = array(); 
			//add html for action
			$row[] = '<a href="deliveryorder/detail/'.$deliveryorder->idcustomer.'" title="Edit")"> '.$deliveryorder->customercode.'</a>';
			$row[] = $deliveryorder->customercode;
			$row[] = $deliveryorder->customername;
			$row[] = $deliveryorder->address1;
			$row[] = $deliveryorder->zipcode;
			$row[] = $deliveryorder->phone1;
			$row[] = $deliveryorder->hp1;
			$row[] = $deliveryorder->fax;
			$row[] = $deliveryorder->emailaddress;
 
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->deliveryorder->count_all(),
			"recordsFiltered" => $this->deliveryorder->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->_set_rules();
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
			deliveryorder.idtrans,
			deliveryorder.codetrans,
			deliveryorder.notrans,
			deliveryorder.salestype,
			DATE_FORMAT(
			deliveryorder.datetrans,
			'%d/%m/%Y'
			)AS datetrans,
			deliveryorder.remark AS remark,
			deliveryorder.customerid,
			deliveryorder.paytypeid, 
			customer.customername,
			paytype.paytypename,
			deliveryorder.shiptoid,
			shipto.shiptoaddress,
			deliveryorder.currency,
			deliveryorder.salesmanid,
			salesman.salesmanname,
			CASE WHEN deliveryorder.status=0 THEN 1 ELSE 0 END AS statussw,
			deliveryorder.status
			FROM
			deliveryorder
			LEFT JOIN customer ON deliveryorder.customerid = customer.idcustomer
			LEFT JOIN paytype ON deliveryorder.paytypeid = paytype.idpaytype
			LEFT JOIN shipto ON deliveryorder.shiptoid = shipto.idshipto
			LEFT JOIN salesman ON deliveryorder.salesmanid = salesman.idsalesman
			WHERE deliveryorder.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result();
			$data['shipto'] = $this->shipto->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result();
			$data['salesman'] = $this->salesman->getdata()->result();

			$this->template->display('deliveryorder/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('deliveryorder',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('deliveryorder');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true),
				'salestype'=>$this->input->post('salestype', true),
				'shiptoid'=>$this->input->post('shiptoid', true),
				'currency'=>$this->input->post('currency', true),
				'salesmanid'=>$this->input->post('salesmanid', true),
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->deliveryorder->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->deliveryorder->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->deliveryorder->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->deliveryorder->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	function _set_rules(){
        $this->form_validation->set_rules('remark','Remark','required|max_length[10]');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');


    }

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		deliveryorderdet
		INNER JOIN inventory ON deliveryorderdet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
		deliveryorderdet.idtransdet,
		deliveryorderdet.transid,
		inventory.inventorycode,
		inventory.inventoryname,
		deliveryorderdet.unit,
		deliveryorderdet.inventoryid,
		FORMAT(deliveryorderdet.unitprice,0) AS unitprice,
		FORMAT(deliveryorderdet.discount,0) AS discount,
		FORMAT(deliveryorderdet.amount,0) AS amount,
		FORMAT(deliveryorderdet.quantity,0) AS quantity
		FROM
		deliveryorderdet
		INNER JOIN inventory ON deliveryorderdet.inventoryid = inventory.idinventory
		WHERE deliveryorderdet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'],
				'unitprice'=>$data['unitprice'],
				'unit'=>$data['unit'],
				'discount'=>$data['discount'],
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(amount),0) AS amount FROM
		deliveryorderdet
		INNER JOIN inventory ON deliveryorderdet.inventoryid = inventory.idinventory
		WHERE deliveryorderdet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->deliveryorder->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->deliveryorder->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->deliveryorder->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
