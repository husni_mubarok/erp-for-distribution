<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class paytype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('paytype_model','paytype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('paytype/index');
	}

	public function ajax_list()
	{
		$list = $this->paytype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $paytype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$paytype->idpaytype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$paytype->idpaytype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$paytype->idpaytype."'".')"> | Delete</a>';
			$row[] = $paytype->paytypename;

			if ($paytype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->paytype->count_all(),
						"recordsFiltered" => $this->paytype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idpaytype)
	{
		$data = $this->paytype->get_by_id($idpaytype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'paytypename' => $this->input->post('paytypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->paytype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'paytypename' => $this->input->post('paytypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->paytype->update(array('idpaytype' => $this->input->post('idpaytype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idpaytype)
	{
		$this->paytype->delete_by_id($idpaytype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idpaytype');
		foreach ($list_id as $idpaytype) {
			$this->paytype->delete_by_id($idpaytype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('paytypename') == '')
		{
			$data['inputerror'][] = 'paytypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
