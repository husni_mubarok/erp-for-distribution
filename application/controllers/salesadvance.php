<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class salesadvance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('salesadvance_model','salesadvance');
		$this->load->model('customer_model', 'customer');
		$this->load->model('shipto_model','shipto');
		$this->load->model('currency_model', 'currency');
		$this->load->model('salesman_model', 'salesman');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
		

			$this->load->helper('url');
			$this->template->display('salesadvance/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO salesadvance (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(salesadvance.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			salesadvance
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('salesadvance/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->salesadvance->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $salesadvance) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$salesadvance->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="salesadvance/detail/'.$salesadvance->idtrans.'" title="Edit")"> '.$salesadvance->notrans.'</a>';
			$row[] = $salesadvance->datetrans;
			$row[] = $salesadvance->customername;
			$row[] = $salesadvance->currency;
			$row[] = $salesadvance->top;
			$row[] = $salesadvance->orderref;
			$row[] = $salesadvance->paytypename;

			if ($salesadvance->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->salesadvance->count_all(),
			"recordsFiltered" => $this->salesadvance->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
			salesadvance.idtrans,
			salesadvance.codetrans,
			salesadvance.notrans, 
			DATE_FORMAT(
			salesadvance.datetrans,
			'%d/%m/%Y'
			)AS datetrans,
			salesadvance.remark AS remark,
			salesadvance.customerid,  
			customer.customername,   
			salesadvance.currency
			FROM
			salesadvance
			LEFT JOIN customer ON salesadvance.customerid = customer.idcustomer  
			WHERE salesadvance.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result();
			$data['shipto'] = $this->shipto->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result();
			$data['salesman'] = $this->salesman->getdata()->result();

			$this->template->display('salesadvance/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('salesadvance',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('salesadvance');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true),
				'currency'=>$this->input->post('currency', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesadvance->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesadvance->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->salesadvance->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->salesadvance->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(salesadvancedet.description,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS amount FROM
		salesadvancedet where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
						salesadvancedet.idtransdet,
						salesadvancedet.transid,
						salesadvancedet.description,
						FORMAT(salesadvancedet.amount, 0) AS amount
					FROM
						salesadvancedet
					WHERE salesadvancedet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows"; 

		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'description'=>$data['description'], 
				'amount'=>$data['amount']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(amount),0) AS amount FROM
		salesadvancedet 
		WHERE salesadvancedet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('description'=>'Total',
				'amount'=>$data['amount']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->salesadvance->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->salesadvance->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->salesadvance->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
