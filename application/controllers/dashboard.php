<?php
class Dashboard extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library(array('form_validation','template'));
        
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
    }
    
    function index(){
        $data['title']="Home";
        
        $this->template->display('dashboard/index',$data);
    }
   
    function _set_rules(){
        $this->form_validation->set_rules('user','username','required|trim');
        $this->form_validation->set_rules('password','password','required|trim');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'>","</div>");
    }
    
    function logout(){
        $this->session->unset_userdata('username');
        redirect('web');
    }
	function adduser(){
        $data['title']="Add User";
        $this->_set_rules();
        if($this->form_validation->run()==true){
            $user=$this->input->post('user');
            $cek=$this->user_model->cekKode($user);
            if($cek->num_rows()>0){
                $data['message']="<div class='alert alert-danger'>Username alredy exists</div>";
                $this->template->display('dashboard/adduser',$data);
            }else{
                $info=array(
                    'user'=>$this->input->post('user'),
                    'password'=>md5($this->input->post('password'))
                );
                $this->user_model->simpan($info);
                redirect('dashboard/adduser/add_success');
            }
        }else{
            $data['message']="";
            $this->template->display('dashboard/adduser',$data);
        }
    }
    function changepassword($id){
        $data['title']="Changepasswoed";
        $this->_set_rules();
        if($this->form_validation->run()==true){
            $info=array(
                'user'=>$this->input->post('user'),
                'password'=>md5($this->input->post('password'))
            );
            $this->user_model->update($id,$info);
            $data['usermain']=$this->user_model->checkusername($id)->row_array();
            $data['message']="<div class='alert alert-success'>Change password success</div>";
            $this->template->display('dashboard/changepassword',$data);
        }else{
            $data['message']="";
            $data['usermain']=$this->user_model->checkusername($id)->row_array();
            $this->template->display('dashboard/changepassword',$data);
        }
    }
    function dashboardwidget(){
        $cari=$this->input->post('cari');
        $cek=$this->document_model->cari($cari);
        if($cek->num_rows()>0){
            $data['message']="";
            $data['document']=$cek->result();
            $this->template->display('document/cari',$data);
        }else{
            $data['message']="<div class='alert alert-success'>Data tidak ditemukan</div>";
            $data['document']=$cek->result();
            $this->template->display('document/cari',$data);
        }
    }

}