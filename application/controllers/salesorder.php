<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class salesorder extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->library(array('template','form_validation'));
		$this->load->model('user_model');
		$this->load->model('salesorder_model','salesorder');
		$this->load->model('customer_model', 'customer');
		$this->load->model('shipto_model','shipto');
		$this->load->model('currency_model', 'currency');
		$this->load->model('salesman_model', 'salesman');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn'); 
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}	
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('salesorder/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO salesorder (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(salesorder.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			salesorder
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('salesorder/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->salesorder->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $salesorder) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$salesorder->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="salesorder/detail/'.$salesorder->idtrans.'" title="Edit")"> '.$salesorder->notrans.'</a>';
			$row[] = $salesorder->datetrans;
			$row[] = $salesorder->customername;
			$row[] = $salesorder->currency;
			$row[] = $salesorder->top;
			$row[] = $salesorder->orderref;
			$row[] = $salesorder->paytypename;

			if ($salesorder->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($salesorder->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($salesorder->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->salesorder->count_all(),
			"recordsFiltered" => $this->salesorder->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->_set_rules();
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
			salesorder.idtrans,
			salesorder.codetrans,
			salesorder.notrans,
			salesorder.salestype,
			DATE_FORMAT(
			salesorder.datetrans,
			'%d/%m/%Y'
			)AS datetrans,
			salesorder.remark AS remark,
			salesorder.customerid,
			salesorder.paytypeid, 
			customer.customername,
			paytype.paytypename,
			salesorder.shiptoid,
			shipto.shiptoaddress,
			salesorder.currency,
			salesorder.salesmanid,
			salesman.salesmanname,
			CASE WHEN salesorder.status=0 THEN 1 ELSE 0 END AS statussw,
			salesorder.status
			FROM
			salesorder
			LEFT JOIN customer ON salesorder.customerid = customer.idcustomer
			LEFT JOIN paytype ON salesorder.paytypeid = paytype.idpaytype
			LEFT JOIN shipto ON salesorder.shiptoid = shipto.idshipto
			LEFT JOIN salesman ON salesorder.salesmanid = salesman.idsalesman
			WHERE salesorder.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result();
			$data['shipto'] = $this->shipto->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result();
			$data['salesman'] = $this->salesman->getdata()->result();

			$this->template->display('salesorder/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('salesorder',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('salesorder');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true),
				'salestype'=>$this->input->post('salestype', true),
				'shiptoid'=>$this->input->post('shiptoid', true),
				'currency'=>$this->input->post('currency', true),
				'salesmanid'=>$this->input->post('salesmanid', true),
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesorder->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesorder->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->salesorder->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->salesorder->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	function _set_rules(){
        $this->form_validation->set_rules('remark','Remark','required|max_length[10]');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');


    }

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		salesorderdet
		INNER JOIN inventory ON salesorderdet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
		salesorderdet.idtransdet,
		salesorderdet.transid,
		inventory.inventorycode,
		inventory.inventoryname,
		salesorderdet.unit,
		salesorderdet.inventoryid,
		FORMAT(salesorderdet.unitprice,0) AS unitprice,
		FORMAT(salesorderdet.discount,0) AS discount,
		FORMAT(salesorderdet.amount,0) AS amount,
		FORMAT(salesorderdet.quantity,0) AS quantity
		FROM
		salesorderdet
		INNER JOIN inventory ON salesorderdet.inventoryid = inventory.idinventory
		WHERE salesorderdet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'],
				'unitprice'=>$data['unitprice'],
				'unit'=>$data['unit'],
				'discount'=>$data['discount'],
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(amount),0) AS amount FROM
		salesorderdet
		INNER JOIN inventory ON salesorderdet.inventoryid = inventory.idinventory
		WHERE salesorderdet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->salesorder->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->salesorder->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->salesorder->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
