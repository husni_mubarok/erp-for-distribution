<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class salesreturn extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('salesreturn_model','salesreturn');
		$this->load->model('customer_model', 'customer');
		$this->load->model('shipto_model','shipto');
		$this->load->model('currency_model', 'currency');
		$this->load->model('salesman_model', 'salesman');
		$this->load->model('salesreturnreason_model', 'salesreturnreason');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
		

			$this->load->helper('url');
			$this->template->display('salesreturn/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO salesreturn (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(salesreturn.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			salesreturn
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('salesreturn/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->salesreturn->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $salesreturn) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$salesreturn->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="salesreturn/detail/'.$salesreturn->idtrans.'" title="Edit")"> '.$salesreturn->notrans.'</a>';
			$row[] = $salesreturn->datetrans;
			$row[] = $salesreturn->customername;
			$row[] = $salesreturn->currency;
			$row[] = $salesreturn->top;
			$row[] = $salesreturn->orderref;
			$row[] = $salesreturn->paytypename;

			if ($salesreturn->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($salesreturn->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($salesreturn->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->salesreturn->count_all(),
			"recordsFiltered" => $this->salesreturn->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
			salesreturn.idtrans,
			salesreturn.codetrans,
			salesreturn.notrans, 
			DATE_FORMAT(
			salesreturn.datetrans,
			'%d/%m/%Y'
			)AS datetrans,
			salesreturn.remark AS remark,
			salesreturn.customerid,
			salesreturn.paytypeid, 
			customer.customername,
			paytype.paytypename,
			salesreturn.shiptoid,
			shipto.shiptoaddress,
			salesreturn.currency,
			salesreturn.salesmanid,
			salesman.salesmanname,
			salesreturn.salesreturnreasonid,
			salesreturnreason.salesreturnreasonname,
			CASE WHEN salesreturn.status=0 THEN 1 ELSE 0 END AS statussw,
			salesreturn.status
			FROM
			salesreturn
			LEFT JOIN customer ON salesreturn.customerid = customer.idcustomer
			LEFT JOIN paytype ON salesreturn.paytypeid = paytype.idpaytype
			LEFT JOIN shipto ON salesreturn.shiptoid = shipto.idshipto
			LEFT JOIN salesman ON salesreturn.salesmanid = salesman.idsalesman
			LEFT JOIN salesreturnreason ON salesreturn.salesreturnreasonid = salesreturnreason.idsalesreturnreason
			WHERE salesreturn.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result();
			$data['shipto'] = $this->shipto->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result();
			$data['salesman'] = $this->salesman->getdata()->result();
			$data['salesreturnreason'] = $this->salesreturnreason->getdata()->result();

			$this->template->display('salesreturn/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('salesreturn',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('salesreturn');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true), 
				'shiptoid'=>$this->input->post('shiptoid', true),
				'currency'=>$this->input->post('currency', true),
				'salesmanid'=>$this->input->post('salesmanid', true),
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesreturn->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesreturn->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->salesreturn->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->salesreturn->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		salesreturndet
		INNER JOIN inventory ON salesreturndet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
		salesreturndet.idtransdet,
		salesreturndet.transid,
		inventory.inventorycode,
		inventory.inventoryname,
		salesreturndet.unit,
		salesreturndet.inventoryid,
		FORMAT(salesreturndet.unitprice,0) AS unitprice,
		FORMAT(salesreturndet.discount,0) AS discount,
		FORMAT(salesreturndet.amount,0) AS amount,
		FORMAT(salesreturndet.quantity,0) AS quantity
		FROM
		salesreturndet
		INNER JOIN inventory ON salesreturndet.inventoryid = inventory.idinventory
		WHERE salesreturndet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'],
				'unitprice'=>$data['unitprice'],
				'unit'=>$data['unit'],
				'discount'=>$data['discount'],
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(amount),0) AS amount FROM
		salesreturndet
		INNER JOIN inventory ON salesreturndet.inventoryid = inventory.idinventory
		WHERE salesreturndet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->salesreturn->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->salesreturn->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->salesreturn->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
