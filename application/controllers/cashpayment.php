<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cashpayment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('cashpayment_model','cashpayment');
		$this->load->model('supplier_model', 'supplier'); 
		$this->load->model('currency_model', 'currency'); 
		$this->load->model('account_model', 'cashaccount');
		$this->load->model('paytype_model', 'paytype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('cashpayment/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO cashpayment (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(cashpayment.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			cashpayment
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('cashpayment/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->cashpayment->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cashpayment) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$cashpayment->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="cashpayment/detail/'.$cashpayment->idtrans.'" title="Edit")"> '.$cashpayment->notrans.'</a>';
			$row[] = $cashpayment->datetrans;
			$row[] = $cashpayment->suppliername;
			$row[] = $cashpayment->paytypename;
			$row[] = $cashpayment->currencyname;
			$row[] = $cashpayment->accountname; 
			$row[] = $cashpayment->remark;

			if ($cashpayment->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($cashpayment->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($cashpayment->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cashpayment->count_all(),
			"recordsFiltered" => $this->cashpayment->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('supplierid','Supplier','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
						cashpayment.idtrans,
						cashpayment.codetrans,
						cashpayment.notrans,
						cashpayment.supplierid,
						supplier.suppliername,
						paytype.paytypename,
						currency.currencyname,
						cashpayment.datetrans,
						DATE_FORMAT(cashpayment.datetrans,'%d/%m/%Y') AS datetrans,
						cashpayment.paytypeid,
						cashpayment.currencyid,
						cashpayment.accountcashid,
						account.accountname,
						cashpayment.amount,
						cashpayment.girono, 
						DATE_FORMAT(cashpayment.girodate,'%d/%m/%Y') AS girodate,
						cashpayment.remark 
					FROM
						cashpayment
					LEFT JOIN supplier ON cashpayment.supplierid = supplier.idsupplier
					LEFT JOIN paytype ON cashpayment.paytypeid = paytype.idpaytype
					LEFT JOIN currency ON cashpayment.currencyid = currency.idcurrency
					LEFT JOIN account ON cashpayment.accountcashid = account.idaccount
			WHERE cashpayment.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['supplier'] = $this->supplier->getdata()->result();
			$data['cashaccount'] = $this->cashaccount->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result(); 
			$data['paytype'] = $this->paytype->getdata()->result(); 

			$this->template->display('cashpayment/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$vargirodate = $this->input->post('girodate', true); 
			$date = str_replace('/', '-', $vargirodate);
			$girodate = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('cashpayment',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('cashpayment');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$vargirodate = $this->input->post('girodate', true); 
		$date = str_replace('/', '-', $vargirodate);
		$girodate = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'supplierid'=>$this->input->post('supplierid', true),
				'paytypeid'=>$this->input->post('paytypeid', true),
				'accountcashid'=>$this->input->post('accountcashid', true),
				'currencyid'=>$this->input->post('currencyid', true),
				'girono'=>$this->input->post('girono', true),
				'girodate'=>$girodate,
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashpayment->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashpayment->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->cashpayment->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('supplierid') == '')
		{
			$data['inputerror'][] = 'supplierid';
			$data['error_string'][] = 'Supplier is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(account.accountname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		cashpaymentdet
					LEFT JOIN account ON cashpaymentdet.accountid = account.idaccount
					LEFT JOIN costcenter ON cashpaymentdet.costcenterid = costcenter.idcostcenter
					LEFT JOIN employee ON cashpaymentdet.costitemid = employee.idemployee where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
					cashpaymentdet.idtransdet,
					cashpaymentdet.transid,
					cashpaymentdet.accountid,
					account.accountcode,
					account.accountname,
					cashpaymentdet.costcenterid,
					costcenter.costcentername,
					cashpaymentdet.costitemid,
					employee.employeename,
					FORMAT(cashpaymentdet.debt,0) AS debt,
					FORMAT(cashpaymentdet.credit,0) AS credit,
					FORMAT(cashpaymentdet.amount,0) AS amount
					FROM
					cashpaymentdet
					LEFT JOIN account ON cashpaymentdet.accountid = account.idaccount
					LEFT JOIN costcenter ON cashpaymentdet.costcenterid = costcenter.idcostcenter
					LEFT JOIN employee ON cashpaymentdet.costitemid = employee.idemployee
					WHERE cashpaymentdet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'accountid'=>$data['accountid'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'],
				'costcenterid'=>$data['costcenterid'],
				'costcentername'=>$data['costcentername'],
				'costitemid'=>$data['costitemid'],
				'employeename'=>$data['employeename'],
				'debt'=>$data['debt'],
				'credit'=>$data['credit'],
				'amount'=>$data['amount']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(amount),0) AS amount, FORMAT(SUM(amount),0) AS amount FROM
		cashpaymentdet
		LEFT JOIN account ON cashpaymentdet.accountid = account.idaccount
		LEFT JOIN costcenter ON cashpaymentdet.costcenterid = costcenter.idcostcenter
		LEFT JOIN employee ON cashpaymentdet.costitemid = employee.idemployee
		WHERE cashpaymentdet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->cashpayment->create_detail($segment)) 
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->cashpayment->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->cashpayment->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
