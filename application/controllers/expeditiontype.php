<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class expeditiontype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('expeditiontype_model','expeditiontype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('expeditiontype/index');
	}

	public function ajax_list()
	{
		$list = $this->expeditiontype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $expeditiontype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$expeditiontype->idexpeditiontype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$expeditiontype->idexpeditiontype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$expeditiontype->idexpeditiontype."'".')"> | Delete</a>';
			$row[] = $expeditiontype->expeditiontypename;

			if ($expeditiontype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->expeditiontype->count_all(),
						"recordsFiltered" => $this->expeditiontype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idexpeditiontype)
	{
		$data = $this->expeditiontype->get_by_id($idexpeditiontype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'expeditiontypename' => $this->input->post('expeditiontypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->expeditiontype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'expeditiontypename' => $this->input->post('expeditiontypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->expeditiontype->update(array('idexpeditiontype' => $this->input->post('idexpeditiontype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idexpeditiontype)
	{
		$this->expeditiontype->delete_by_id($idexpeditiontype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idexpeditiontype');
		foreach ($list_id as $idexpeditiontype) {
			$this->expeditiontype->delete_by_id($idexpeditiontype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('expeditiontypename') == '')
		{
			$data['inputerror'][] = 'expeditiontypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
