<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stockopname extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('stockopname_model','stockopname');
		$this->load->model('matusedtype_model', 'matusedtype');
		$this->load->model('warehouse_model','warehouse'); 
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
		

			$this->load->helper('url');
			$this->template->display('stockopname/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO stockopname (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(stockopname.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			stockopname
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('stockopname/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->stockopname->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stockopname) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$stockopname->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="stockopname/detail/'.$stockopname->idtrans.'" title="Edit")"> '.$stockopname->notrans.'</a>';
			$row[] = $stockopname->datetrans;
			$row[] = $stockopname->matusedtypename;
			$row[] = $stockopname->warehousename;
			$row[] = $stockopname->remark; 

			if ($stockopname->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->stockopname->count_all(),
			"recordsFiltered" => $this->stockopname->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('matusedtypeid','Used Type','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
				stockopname.idtrans,
				stockopname.codetrans,
				stockopname.notrans,
				DATE_FORMAT(
					stockopname.datetrans,
					'%d/%m/%Y'
				) AS datetrans,
				stockopname.matusedtypeid,
				matusedtype.matusedtypename,  
				warehouse.warehousecode,
				warehouse.warehousename,
				stockopname.remark
			FROM
				stockopname
			LEFT JOIN matusedtype ON stockopname.matusedtypeid = matusedtype.idmatusedtype
			LEFT JOIN warehouse ON stockopname.warehouseid = warehouse.idwarehouse
			WHERE stockopname.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['matusedtype'] = $this->matusedtype->getdata()->result();
			$data['warehouse'] = $this->warehouse->getdata()->result(); 

			$this->template->display('stockopname/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('stockopname',array(
				'datetrans'=>$datetrans,
				'matusedtypeid'=>$this->input->post('matusedtypeid', true),
				'warehouseid'=>$this->input->post('warehouseid', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('stockopname');
		}
	}

	public function getstock($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('matusedtypeid','Used Type','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === TRUE)
		{
			$query_str="SELECT
				stockopname.idtrans,
				stockopname.codetrans,
				stockopname.notrans,
				DATE_FORMAT(
					stockopname.datetrans,
					'%d/%m/%Y'
				) AS datetrans,
				stockopname.matusedtypeid,
				matusedtype.matusedtypename,  
				warehouse.warehousecode,
				warehouse.warehousename,
				stockopname.remark
			FROM
				stockopname
			LEFT JOIN matusedtype ON stockopname.matusedtypeid = matusedtype.idmatusedtype
			LEFT JOIN warehouse ON stockopname.warehouseid = warehouse.idwarehouse
			WHERE stockopname.idtrans= ". $id ."";

		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'matusedtypeid'=>$this->input->post('matusedtypeid', true),
				'warehouseid'=>$this->input->post('warehouseid', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->stockopname->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->stockopname->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->stockopname->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->stockopname->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('matusedtypeid') == '')
		{
			$data['inputerror'][] = 'matusedtypeid';
			$data['error_string'][] = 'Uded type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		stockopnamedet
		INNER JOIN inventory ON stockopnamedet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
			stockopnamedet.idtransdet,
			stockopnamedet.transid,
			stockopnamedet.inventoryid,
			inventory.inventorycode,
			inventory.inventoryname,
			stockopnamedet.unit,
			FORMAT(stockopnamedet.quantity,0) AS quantity,
			FORMAT(stockopnamedet.stockqty,0) AS stockqty,
			FORMAT(stockopnamedet.adjqty,0) AS adjqty
		FROM
			stockopnamedet
		LEFT JOIN inventory ON stockopnamedet.inventoryid = inventory.idinventory
		WHERE stockopnamedet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'], 
				'unit'=>$data['unit'], 
				'quantity'=>$data['quantity'], 
				'stockqty'=>$data['stockqty'], 
				'adjqty'=>$data['adjqty']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(stockqty),0) AS stockqty, FORMAT(SUM(adjqty),0) AS adjqty FROM
		stockopnamedet
		INNER JOIN inventory ON stockopnamedet.inventoryid = inventory.idinventory
		WHERE stockopnamedet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unit'=>'Total', 
				'quantity'=>$data['quantity'],
				'stockqty'=>$data['stockqty'],
				'adjqty'=>$data['adjqty']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->stockopname->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->stockopname->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->stockopname->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
