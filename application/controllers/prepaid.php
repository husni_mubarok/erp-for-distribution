<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class prepaid extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('prepaid_model','prepaid'); 
		$this->load->model('account_model', 'account');  
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('prepaid/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO prepaid (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(prepaid.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			prepaid
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('prepaid/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->prepaid->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $prepaid) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$prepaid->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="prepaid/detail/'.$prepaid->idtrans.'" title="Edit")"> '.$prepaid->notrans.'</a>';
			$row[] = $prepaid->datetrans;
			$row[] = $prepaid->prepaidname;
			$row[] = $prepaid->accountname;
			$row[] = $prepaid->cost;
			$row[] = $prepaid->usefullife;
			$row[] = $prepaid->beginamount;  
			$row[] = $prepaid->remark;

			if ($prepaid->status == 0) {
				$row[] = '<span class="label label-success"> Released </span>';
			}else{
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->prepaid->count_all(),
			"recordsFiltered" => $this->prepaid->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('currencyid','Currency','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
						prepaid.idtrans,
						prepaid.codetrans,
						prepaid.notrans,
						DATE_FORMAT(prepaid.datetrans,'%d/%m/%Y') AS datetrans,
						prepaid.prepaidname,
						prepaid.accountid,
						account.accountname,
						prepaid.cost,
						prepaid.usefullife,
						prepaid.beginamount,
						prepaid.remark,
						prepaid.`status`
						FROM
						prepaid
						LEFT JOIN account ON prepaid.accountid = account.idaccount
						WHERE prepaid.idtrans= ". $id ."";
			$data['detail'] = $this->db->query($query_str)->row_array(); 
			$data['account'] = $this->account->getdata()->result(); 

			$this->template->display('prepaid/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date)); 

			$varreversedate = $this->input->post('reversedate', true); 
			$datereversedate = str_replace('/', '-', $varreversedate);
			$reversedate = date('Y-m-d', strtotime($datereversedate)); 

			$this->db->where('idtrans', $id);
			$this->db->update('prepaid',array(
				'datetrans'=>$datetrans, 
				'reversedate'=>$reversedate, 
				'currencyid'=>$this->input->post('currencyid', true), 
				'noref'=>$this->input->post('noref', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('prepaid');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));  

		$varreversedate = $this->input->post('reversedate', true); 
		$datereversedate = str_replace('/', '-', $varreversedate);
		$reversedate = date('Y-m-d', strtotime($datereversedate)); 

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans, 
				'reversedate'=>$reversedate, 
				'currencyid'=>$this->input->post('currencyid', true), 
				'noref'=>$this->input->post('noref', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->prepaid->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->prepaid->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->prepaid->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('currencyid') == '')
		{
			$data['inputerror'][] = 'currencyid';
			$data['error_string'][] = 'Currency is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(account.accountname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		prepaiddet
					LEFT JOIN account ON prepaiddet.accountid = account.idaccount
					LEFT JOIN costcenter ON prepaiddet.costcenterid = costcenter.idcostcenter where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
					prepaiddet.idtransdet,
					prepaiddet.transid,
					prepaiddet.accountid,
					account.accountcode,
					account.accountname,
					prepaiddet.costcenterid,
					costcenter.costcentername,  
					FORMAT(prepaiddet.debt,0) AS debt,
					FORMAT(prepaiddet.credit,0) AS credit 
					FROM
					prepaiddet
					LEFT JOIN account ON prepaiddet.accountid = account.idaccount
					LEFT JOIN costcenter ON prepaiddet.costcenterid = costcenter.idcostcenter 
					WHERE prepaiddet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'accountid'=>$data['accountid'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'],
				'costcenterid'=>$data['costcenterid'],
				'costcentername'=>$data['costcentername'], 
				'debt'=>$data['debt'],
				'credit'=>$data['credit'] 
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(debt),0) AS debt, FORMAT(SUM(credit),0) AS credit FROM
		prepaiddet
		LEFT JOIN account ON prepaiddet.accountid = account.idaccount
		LEFT JOIN costcenter ON prepaiddet.costcenterid = costcenter.idcostcenter 
		WHERE prepaiddet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('description'=>'Total',
				'debt'=>$data['debt'],
				'credit'=>$data['credit']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->prepaid->create_detail($segment)) 
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->prepaid->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->prepaid->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
