<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shipto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('shipto_model','shipto');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('shipto/index');
	}

	public function ajax_list()
	{
		$list = $this->shipto->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $shipto) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$shipto->idshipto.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$shipto->idshipto."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$shipto->idshipto."'".')"> | Delete</a>';
			$row[] = $shipto->shiptoname;
			$row[] = $shipto->shiptoaddress;

			if ($shipto->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->shipto->count_all(),
						"recordsFiltered" => $this->shipto->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idshipto)
	{
		$data = $this->shipto->get_by_id($idshipto);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'shiptoname' => $this->input->post('shiptoname'),
				'shiptoaddress' => $this->input->post('shiptoaddress'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->shipto->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'shiptoname' => $this->input->post('shiptoname'),
				'shiptoaddress' => $this->input->post('shiptoaddress'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->shipto->update(array('idshipto' => $this->input->post('idshipto')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idshipto)
	{
		$this->shipto->delete_by_id($idshipto);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idshipto');
		foreach ($list_id as $idshipto) {
			$this->shipto->delete_by_id($idshipto);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('shiptoname') == '')
		{
			$data['inputerror'][] = 'shiptoname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


}
