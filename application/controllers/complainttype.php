<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class complainttype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('complainttype_model','complainttype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('complainttype/index');
	}

	public function ajax_list()
	{
		$list = $this->complainttype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $complainttype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$complainttype->idcomplainttype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$complainttype->idcomplainttype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$complainttype->idcomplainttype."'".')"> | Delete</a>';
			$row[] = $complainttype->complainttypename;

			if ($complainttype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->complainttype->count_all(),
						"recordsFiltered" => $this->complainttype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcomplainttype)
	{
		$data = $this->complainttype->get_by_id($idcomplainttype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'complainttypename' => $this->input->post('complainttypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->complainttype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'complainttypename' => $this->input->post('complainttypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->complainttype->update(array('idcomplainttype' => $this->input->post('idcomplainttype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcomplainttype)
	{
		$this->complainttype->delete_by_id($idcomplainttype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcomplainttype');
		foreach ($list_id as $idcomplainttype) {
			$this->complainttype->delete_by_id($idcomplainttype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('complainttypename') == '')
		{
			$data['inputerror'][] = 'complainttypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
