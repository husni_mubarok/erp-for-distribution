<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class department extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('department_model','department');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('department/index');
	}

	public function ajax_list()
	{
		$list = $this->department->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $department) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$department->iddepartment.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$department->iddepartment."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$department->iddepartment."'".')"> | Delete</a>';
			$row[] = $department->departmentname;

			if ($department->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->department->count_all(),
						"recordsFiltered" => $this->department->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($iddepartment)
	{
		$data = $this->department->get_by_id($iddepartment);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'departmentname' => $this->input->post('departmentname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->department->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'departmentname' => $this->input->post('departmentname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->department->update(array('iddepartment' => $this->input->post('iddepartment')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($iddepartment)
	{
		$this->department->delete_by_id($iddepartment);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('iddepartment');
		foreach ($list_id as $iddepartment) {
			$this->department->delete_by_id($iddepartment);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('departmentname') == '')
		{
			$data['inputerror'][] = 'departmentname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
