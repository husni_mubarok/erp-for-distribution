<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class employee extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('employee_model','employee');
		$this->load->model('position_model', 'position');
		$this->load->model('department_model', 'department');
		$this->load->model('educationlevel_model', 'educationlevel');
		$this->load->model('location_model', 'location');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$data['position']=$this->position->getdata()->result();
		$data['department']=$this->department->getdata()->result();
		$data['educationlevel']=$this->educationlevel->getdata()->result();
		$data['location']=$this->location->getdata()->result();
		//var_dump($data);
		$this->template->display('employee/index', $data);
	}

	public function ajax_list()
	{
		$list = $this->employee->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $employee) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$employee->idemployee.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$employee->idemployee."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$employee->idemployee."'".')"> | Delete</a>';
			$row[] = $employee->nik;
			$row[] = $employee->identityno;
			$row[] = $employee->employeename;
			$row[] = $employee->nickname;
			$row[] = $employee->address;
			$row[] = $employee->npwp;
			$row[] = $employee->sex;

			if ($employee->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->employee->count_all(),
						"recordsFiltered" => $this->employee->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idemployee)
	{
		$data = $this->employee->get_by_id($idemployee);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'nik' => $this->input->post('nik'),
				'identityno' => $this->input->post('identityno'),
				'employeename' => $this->input->post('employeename'),
				'nickname' => $this->input->post('nickname'),
				'placebirth' => $this->input->post('placebirth'),
				'datebirth' => $this->input->post('datebirth'),
				'address' => $this->input->post('address'),
				'maritalstatus' => $this->input->post('maritalstatus'),
				'sex' => $this->input->post('sex'),
				'joindate' => $this->input->post('joindate'),
				'npwp' => $this->input->post('npwp'),
				'positionid' => $this->input->post('positionid'),
				'departmentid' => $this->input->post('departmentid'),
				'educationlevelid' => $this->input->post('educationlevelid'),
				'locationid' => $this->input->post('locationid'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->employee->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'nik' => $this->input->post('nik'),
				'identityno' => $this->input->post('identityno'),
				'employeename' => $this->input->post('employeename'),
				'nickname' => $this->input->post('nickname'),
				'placebirth' => $this->input->post('placebirth'),
				'datebirth' => $this->input->post('datebirth'),
				'address' => $this->input->post('address'),
				'maritalstatus' => $this->input->post('maritalstatus'),
				'sex' => $this->input->post('sex'),
				'joindate' => $this->input->post('joindate'),
				'npwp' => $this->input->post('npwp'),
				'positionid' => $this->input->post('positionid'),
				'departmentid' => $this->input->post('departmentid'),
				'educationlevelid' => $this->input->post('educationlevelid'),
				'locationid' => $this->input->post('locationid'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->employee->update(array('idemployee' => $this->input->post('idemployee')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idemployee)
	{
		$this->employee->delete_by_id($idemployee);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idemployee');
		foreach ($list_id as $idemployee) {
			$this->employee->delete_by_id($idemployee);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('employeename') == '')
		{
			$data['inputerror'][] = 'employeename';
			$data['error_string'][] = 'Employee name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
