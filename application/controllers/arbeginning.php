<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class arbeginning extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('customer_model','customer'); 
		$this->load->model('arbeginning_model', 'arbeginning');
		$this->load->model('currency_model', 'currency');
		$this->load->model('account_model', 'account');
		$this->load->model('top_model', 'top');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('arbeginning/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO arinvoice (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(arinvoice.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			arinvoice
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('arbeginning/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->arbeginning->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $arinvoice) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$arinvoice->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="arbeginning/detail/'.$arinvoice->idtrans.'" title="Edit")"> '.$arinvoice->notrans.'</a>';
			$row[] = $arinvoice->datetrans;
			$row[] = $arinvoice->accountfrom;
			$row[] = $arinvoice->accountto;
			$row[] = $arinvoice->amount;
			$row[] = $arinvoice->rate;
			$row[] = $arinvoice->charge; 
			$row[] = $arinvoice->remark;

			if ($arinvoice->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($arinvoice->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($arinvoice->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->arbeginning->count_all(),
			"recordsFiltered" => $this->arbeginning->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
							arinvoice.idtrans,
							arinvoice.codetrans,
							arinvoice.notrans,
							DATE_FORMAT(
								arinvoice.datetrans,
								'%d/%m/%Y'
							) AS datetrans,
							arinvoice.amount,
							arinvoice.remark,
							arinvoice.`status`,
							arinvoice.customerid,
							customer.customername, 
							DATE_FORMAT(
								arinvoice.duedate,
								'%d/%m/%Y'
							) AS duedate,
							arinvoice.top,
							arinvoice.currency,
							arinvoice.beginamt,
							arinvoice.settleamt,
							arinvoice.remainamt,
							arinvoice.invoiceref
						FROM
							arinvoice
						LEFT JOIN customer ON arinvoice.customerid = customer.idcustomer
						WHERE arinvoice.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result(); 
			$data['currency'] = $this->currency->getdata()->result();
			$data['account'] = $this->account->getdata()->result();
			$data['top'] = $this->top->getdata()->result();

			$this->template->display('arbeginning/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');
 
			$this->db->where('idtrans', $id);
			$this->db->update('arinvoice',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('arinvoice');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$varduedate = $this->input->post('duedate', true); 
		$date = str_replace('/', '-', $varduedate);
		$duedate = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true),
				'currency'=>$this->input->post('currency', true),
				'accountid'=>$this->input->post('accountid', true),
				'top'=>$this->input->post('top', true),
				'invoiceref'=>$this->input->post('invoiceref', true), 
				'duedate'=>$duedate,
				'rate'=>$this->input->post('rate', true), 
				'beginamt'=>$this->input->post('beginamt', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->arinvoice->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->arinvoice->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->arinvoice->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer from is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}
