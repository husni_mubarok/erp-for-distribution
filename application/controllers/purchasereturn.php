<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class purchasereturn extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('purchasereturn_model','purchasereturn');
		$this->load->model('supplier_model', 'supplier');
		$this->load->model('shipto_model','shipto');
		$this->load->model('currency_model', 'currency');
		$this->load->model('salesman_model', 'salesman');
		$this->load->model('salesreturnreason_model', 'salesreturnreason');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
		

			$this->load->helper('url');
			$this->template->display('purchasereturn/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO purchasereturn (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(purchasereturn.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			purchasereturn
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('purchasereturn/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->purchasereturn->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $purchasereturn) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$purchasereturn->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="purchasereturn/detail/'.$purchasereturn->idtrans.'" title="Edit")"> '.$purchasereturn->notrans.'</a>';
			$row[] = $purchasereturn->datetrans;
			$row[] = $purchasereturn->suppliername;
			$row[] = $purchasereturn->currency;
			$row[] = $purchasereturn->top;
			$row[] = $purchasereturn->orderref;
			$row[] = $purchasereturn->paytypename;

			if ($purchasereturn->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($purchasereturn->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($purchasereturn->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->purchasereturn->count_all(),
			"recordsFiltered" => $this->purchasereturn->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('supplierid','Supplier','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
			purchasereturn.idtrans,
			purchasereturn.codetrans,
			purchasereturn.notrans, 
			DATE_FORMAT(
			purchasereturn.datetrans,
			'%d/%m/%Y'
			)AS datetrans,
			purchasereturn.remark AS remark,
			purchasereturn.supplierid,
			purchasereturn.paytypeid, 
			supplier.suppliername,
			paytype.paytypename,
			purchasereturn.shiptoid,
			shipto.shiptoaddress,
			purchasereturn.currency,   
			CASE WHEN purchasereturn.status=0 THEN 1 ELSE 0 END AS statussw,
			purchasereturn.status
			FROM
			purchasereturn
			LEFT JOIN supplier ON purchasereturn.supplierid = supplier.idsupplier
			LEFT JOIN paytype ON purchasereturn.paytypeid = paytype.idpaytype
			LEFT JOIN shipto ON purchasereturn.shiptoid = shipto.idshipto  
			WHERE purchasereturn.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['supplier'] = $this->supplier->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result(); 

			$this->template->display('purchasereturn/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('purchasereturn',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('purchasereturn');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'supplierid'=>$this->input->post('supplierid', true), 
				'currency'=>$this->input->post('currency', true), 
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->purchasereturn->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->purchasereturn->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->purchasereturn->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->purchasereturn->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('supplierid') == '')
		{
			$data['inputerror'][] = 'supplierid';
			$data['error_string'][] = 'Supplier is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		purchasereturndet
		INNER JOIN inventory ON purchasereturndet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
		purchasereturndet.idtransdet,
		purchasereturndet.transid,
		inventory.inventorycode,
		inventory.inventoryname,
		purchasereturndet.unit,
		purchasereturndet.inventoryid,
		FORMAT(purchasereturndet.unitprice,0) AS unitprice,
		FORMAT(purchasereturndet.discount,0) AS discount,
		FORMAT(purchasereturndet.amount,0) AS amount,
		FORMAT(purchasereturndet.quantity,0) AS quantity
		FROM
		purchasereturndet
		INNER JOIN inventory ON purchasereturndet.inventoryid = inventory.idinventory
		WHERE purchasereturndet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'],
				'unitprice'=>$data['unitprice'],
				'unit'=>$data['unit'],
				'discount'=>$data['discount'],
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(amount),0) AS amount FROM
		purchasereturndet
		INNER JOIN inventory ON purchasereturndet.inventoryid = inventory.idinventory
		WHERE purchasereturndet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->purchasereturn->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->purchasereturn->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->purchasereturn->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
