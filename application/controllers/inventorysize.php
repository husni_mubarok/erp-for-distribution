<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventorysize extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventorysize_model','inventorysize');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('inventorysize/index');
	}

	public function ajax_list()
	{
		$list = $this->inventorysize->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventorysize) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventorysize->idinventorysize.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventorysize->idinventorysize."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventorysize->idinventorysize."'".')"> | Delete</a>';
			$row[] = $inventorysize->inventorysizename;

			if ($inventorysize->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventorysize->count_all(),
						"recordsFiltered" => $this->inventorysize->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventorysize)
	{
		$data = $this->inventorysize->get_by_id($idinventorysize);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'inventorysizename' => $this->input->post('inventorysizename'),
				'status' => $this->input->post('status'),
			);
		$insert = $this->inventorysize->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'inventorysizename' => $this->input->post('inventorysizename'),
				'status' => $this->input->post('status'),
			);
		$this->inventorysize->update(array('idinventorysize' => $this->input->post('idinventorysize')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventorysize)
	{
		$this->inventorysize->delete_by_id($idinventorysize);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventorysize');
		foreach ($list_id as $idinventorysize) {
			$this->inventorysize->delete_by_id($idinventorysize);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventorysizename') == '')
		{
			$data['inputerror'][] = 'inventorysizename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
