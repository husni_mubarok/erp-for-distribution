<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cashreceipt extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('cashreceipt_model','cashreceipt');
		$this->load->model('customer_model', 'customer'); 
		$this->load->model('currency_model', 'currency'); 
		$this->load->model('account_model', 'cashaccount');
		$this->load->model('paytype_model', 'paytype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('url');
			$this->template->display('cashreceipt/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO cashreceipt (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(cashreceipt.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			cashreceipt
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('cashreceipt/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->cashreceipt->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cashreceipt) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$cashreceipt->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="cashreceipt/detail/'.$cashreceipt->idtrans.'" title="Edit")"> '.$cashreceipt->notrans.'</a>';
			$row[] = $cashreceipt->datetrans;
			$row[] = $cashreceipt->customername;
			$row[] = $cashreceipt->paytypename;
			$row[] = $cashreceipt->currencyname;
			$row[] = $cashreceipt->accountname; 
			$row[] = $cashreceipt->remark;

			if ($cashreceipt->status == 0) {
				$row[] = '<span class="label label-warning"> Open </span>';
			}elseif ($cashreceipt->status == 1) {
				$row[] = '<span class="label label-success"> Released </span>';
			}elseif ($cashreceipt->status == 9) {
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cashreceipt->count_all(),
			"recordsFiltered" => $this->cashreceipt->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('customerid','Customer','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
						cashreceipt.idtrans,
						cashreceipt.codetrans,
						cashreceipt.notrans,
						cashreceipt.customerid,
						customer.customername,
						paytype.paytypename,
						currency.currencyname,
						cashreceipt.datetrans,
						DATE_FORMAT(cashreceipt.datetrans,'%d/%m/%Y') AS datetrans,
						cashreceipt.paytypeid,
						cashreceipt.currencyid,
						cashreceipt.accountcashid,
						account.accountname,
						cashreceipt.amount,
						cashreceipt.girono, 
						DATE_FORMAT(cashreceipt.girodate,'%d/%m/%Y') AS girodate,
						cashreceipt.remark 
					FROM
						cashreceipt
					LEFT JOIN customer ON cashreceipt.customerid = customer.idcustomer
					LEFT JOIN paytype ON cashreceipt.paytypeid = paytype.idpaytype
					LEFT JOIN currency ON cashreceipt.currencyid = currency.idcurrency
					LEFT JOIN account ON cashreceipt.accountcashid = account.idaccount
			WHERE cashreceipt.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['customer'] = $this->customer->getdata()->result();
			$data['cashaccount'] = $this->cashaccount->getdata()->result();
			$data['currency'] = $this->currency->getdata()->result(); 
			$data['paytype'] = $this->paytype->getdata()->result(); 

			$this->template->display('cashreceipt/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$vargirodate = $this->input->post('girodate', true); 
			$date = str_replace('/', '-', $vargirodate);
			$girodate = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('cashreceipt',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('cashreceipt');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$vargirodate = $this->input->post('girodate', true); 
		$date = str_replace('/', '-', $vargirodate);
		$girodate = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'customerid'=>$this->input->post('customerid', true),
				'paytypeid'=>$this->input->post('paytypeid', true),
				'accountcashid'=>$this->input->post('accountcashid', true),
				'currencyid'=>$this->input->post('currencyid', true),
				'girono'=>$this->input->post('girono', true),
				'girodate'=>$girodate,
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashreceipt->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->cashreceipt->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->cashreceipt->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('customerid') == '')
		{
			$data['inputerror'][] = 'customerid';
			$data['error_string'][] = 'Customer is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(account.accountname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		cashreceiptdet
					LEFT JOIN account ON cashreceiptdet.accountid = account.idaccount
					LEFT JOIN costcenter ON cashreceiptdet.costcenterid = costcenter.idcostcenter
					LEFT JOIN employee ON cashreceiptdet.costitemid = employee.idemployee where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
					cashreceiptdet.idtransdet,
					cashreceiptdet.transid,
					cashreceiptdet.accountid,
					account.accountcode,
					account.accountname,
					cashreceiptdet.costcenterid,
					costcenter.costcentername,
					cashreceiptdet.costitemid,
					employee.employeename,
					FORMAT(cashreceiptdet.debt,0) AS debt,
					FORMAT(cashreceiptdet.credit,0) AS credit,
					FORMAT(cashreceiptdet.amount,0) AS amount
					FROM
					cashreceiptdet
					LEFT JOIN account ON cashreceiptdet.accountid = account.idaccount
					LEFT JOIN costcenter ON cashreceiptdet.costcenterid = costcenter.idcostcenter
					LEFT JOIN employee ON cashreceiptdet.costitemid = employee.idemployee
					WHERE cashreceiptdet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'accountid'=>$data['accountid'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'],
				'costcenterid'=>$data['costcenterid'],
				'costcentername'=>$data['costcentername'],
				'costitemid'=>$data['costitemid'],
				'employeename'=>$data['employeename'],
				'debt'=>$data['debt'],
				'credit'=>$data['credit'],
				'amount'=>$data['amount']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(amount),0) AS amount, FORMAT(SUM(amount),0) AS amount FROM
		cashreceiptdet
		LEFT JOIN account ON cashreceiptdet.accountid = account.idaccount
		LEFT JOIN costcenter ON cashreceiptdet.costcenterid = costcenter.idcostcenter
		LEFT JOIN employee ON cashreceiptdet.costitemid = employee.idemployee
		WHERE cashreceiptdet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('unitprice'=>'Total',
				'amount'=>$data['amount'],
				'quantity'=>$data['quantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->cashreceipt->create_detail($segment)) 
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->cashreceipt->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->cashreceipt->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
