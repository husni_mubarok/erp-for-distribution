<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class country extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('country_model','country');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('country/index');
	}

	public function ajax_list()
	{
		$list = $this->country->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $country) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$country->idcountry.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$country->idcountry."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$country->idcountry."'".')"> | Delete</a>';
			$row[] = $country->countryname;

			if ($country->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->country->count_all(),
						"recordsFiltered" => $this->country->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcountry)
	{
		$data = $this->country->get_by_id($idcountry);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'countryname' => $this->input->post('countryname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->country->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'countryname' => $this->input->post('countryname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->country->update(array('idcountry' => $this->input->post('idcountry')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcountry)
	{
		$this->country->delete_by_id($idcountry);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcountry');
		foreach ($list_id as $idcountry) {
			$this->country->delete_by_id($idcountry);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('countryname') == '')
		{
			$data['inputerror'][] = 'countryname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
