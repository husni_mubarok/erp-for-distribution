<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class salesman extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('salesman_model','salesman');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('salesman/index');
	}

	public function ajax_list()
	{
		$list = $this->salesman->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $salesman) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$salesman->idsalesman.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$salesman->idsalesman."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$salesman->idsalesman."'".')"> | Delete</a>';
			$row[] = $salesman->salesmancode;
			$row[] = $salesman->salesmanname;

			if ($salesman->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->salesman->count_all(),
						"recordsFiltered" => $this->salesman->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idsalesman)
	{
		$data = $this->salesman->get_by_id($idsalesman);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'salesmancode' => $this->input->post('salesmancode'),
				'salesmanname' => $this->input->post('salesmanname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->salesman->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'salesmancode' => $this->input->post('salesmancode'),
				'salesmanname' => $this->input->post('salesmanname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->salesman->update(array('idsalesman' => $this->input->post('idsalesman')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idsalesman)
	{
		$this->salesman->delete_by_id($idsalesman);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idsalesman');
		foreach ($list_id as $idsalesman) {
			$this->salesman->delete_by_id($idsalesman);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('salesmanname') == '')
		{
			$data['inputerror'][] = 'salesmanname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
