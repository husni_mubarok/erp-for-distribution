<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class warehouse extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('warehouse_model','warehouse');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('warehouse/index');
	}

	public function ajax_list()
	{
		$list = $this->warehouse->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $warehouse) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$warehouse->idwarehouse.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$warehouse->idwarehouse."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$warehouse->idwarehouse."'".')"> | Delete</a>';
			$row[] = $warehouse->warehousecode;
			$row[] = $warehouse->warehousename;

			if ($warehouse->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->warehouse->count_all(),
						"recordsFiltered" => $this->warehouse->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idwarehouse)
	{
		$data = $this->warehouse->get_by_id($idwarehouse);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'warehousename' => $this->input->post('warehousename'),
				'warehousecode' => $this->input->post('warehousecode'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->warehouse->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'warehousename' => $this->input->post('warehousename'),
				'warehousecode' => $this->input->post('warehousecode'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->warehouse->update(array('idwarehouse' => $this->input->post('idwarehouse')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idwarehouse)
	{
		$this->warehouse->delete_by_id($idwarehouse);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idwarehouse');
		foreach ($list_id as $idwarehouse) {
			$this->warehouse->delete_by_id($idwarehouse);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('warehousename') == '')
		{
			$data['inputerror'][] = 'warehousename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
