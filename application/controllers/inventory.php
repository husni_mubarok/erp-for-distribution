<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventory extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventory_model','inventory');
		$this->load->model('inventorybrand_model', 'inventorybrand');
		$this->load->model('inventorycolor_model', 'inventorycolor');
		$this->load->model('inventorygroup_model', 'inventorygroup');
		$this->load->model('inventorytype_model', 'inventorytype');
		$this->load->model('inventorysize_model', 'inventorysize');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$data['inventorybrand']=$this->inventorybrand->getdata()->result();
		$data['inventorycolor']=$this->inventorycolor->getdata()->result();
		$data['inventorygroup']=$this->inventorygroup->getdata()->result();
		$data['inventorytype']=$this->inventorytype->getdata()->result();
		$data['inventorysize']=$this->inventorysize->getdata()->result();
		//var_dump($data);
		$this->template->display('inventory/index', $data);
	}

	public function ajax_list()
	{
		$list = $this->inventory->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventory) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventory->idinventory.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventory->idinventory."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventory->idinventory."'".')"> | Delete</a>';
			$row[] = $inventory->inventorycode;
			$row[] = $inventory->inventoryname; 
			$row[] = $inventory->unitprice;
			$row[] = $inventory->floorprice;
			$row[] = $inventory->inventorybrandname;
			$row[] = $inventory->inventorycolorname;
			$row[] = $inventory->inventorygroupname;
			$row[] = $inventory->inventorytypename;

			if ($inventory->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventory->count_all(),
						"recordsFiltered" => $this->inventory->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventory)
	{
		$data = $this->inventory->get_by_id($idinventory);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorycode' => $this->input->post('inventorycode'),
				'inventoryname' => $this->input->post('inventoryname'),
				'spec' => $this->input->post('spec'),
				'weight' => $this->input->post('weight'),
				'unitprice' => $this->input->post('unitprice'),
				'floorprice' => $this->input->post('floorprice'),
				'inventorybrandcode' => $this->input->post('inventorybrandcode'),
				'inventorygroupid' => $this->input->post('inventorygroupid'),
				'inventorytypeid' => $this->input->post('inventorytypeid'),
				'inventorycolorid' => $this->input->post('inventorycolorid'),
				'inventorysizeid' => $this->input->post('inventorysizeid'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->inventory->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorycode' => $this->input->post('inventorycode'),
				'inventoryname' => $this->input->post('inventoryname'),
				'spec' => $this->input->post('spec'),
				'weight' => $this->input->post('weight'),
				'unitprice' => $this->input->post('unitprice'),
				'floorprice' => $this->input->post('floorprice'),
				'inventorybrandcode' => $this->input->post('inventorybrandcode'),
				'inventorygroupid' => $this->input->post('inventorygroupid'),
				'inventorytypeid' => $this->input->post('inventorytypeid'),
				'inventorycolorid' => $this->input->post('inventorycolorid'),
				'inventorysizeid' => $this->input->post('inventorysizeid'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->inventory->update(array('idinventory' => $this->input->post('idinventory')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventory)
	{
		$this->inventory->delete_by_id($idinventory);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventory');
		foreach ($list_id as $idinventory) {
			$this->inventory->delete_by_id($idinventory);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventoryname') == '')
		{
			$data['inputerror'][] = 'inventoryname';
			$data['error_string'][] = 'Inventory name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
