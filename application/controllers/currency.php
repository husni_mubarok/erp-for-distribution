<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class currency extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('currency_model','currency');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('currency/index');
	}

	public function ajax_list()
	{
		$list = $this->currency->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $currency) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$currency->idcurrency.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$currency->idcurrency."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$currency->idcurrency."'".')"> | Delete</a>';
			$row[] = $currency->currencyname;
			$row[] = $currency->description;

			if ($currency->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->currency->count_all(),
						"recordsFiltered" => $this->currency->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcurrency)
	{
		$data = $this->currency->get_by_id($idcurrency);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'currencyname' => $this->input->post('currencyname'),
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->currency->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'currencyname' => $this->input->post('currencyname'),
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->currency->update(array('idcurrency' => $this->input->post('idcurrency')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcurrency)
	{
		$this->currency->delete_by_id($idcurrency);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcurrency');
		foreach ($list_id as $idcurrency) {
			$this->currency->delete_by_id($idcurrency);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('currencyname') == '')
		{
			$data['inputerror'][] = 'currencyname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
