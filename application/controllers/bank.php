<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bank extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('bank_model','bank');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('bank/index');
	}

	public function ajax_list()
	{
		$list = $this->bank->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $bank) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$bank->idbank.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$bank->idbank."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$bank->idbank."'".')"> | Delete</a>';
			$row[] = $bank->bankname;
			$row[] = $bank->bankaccount;
			$row[] = $bank->banknumber;
			$row[] = $bank->branch;
			if ($bank->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->bank->count_all(),
						"recordsFiltered" => $this->bank->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idbank)
	{
		$data = $this->bank->get_by_id($idbank);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'bankname' => $this->input->post('bankname'),
				'bankaccount	' => $this->input->post('bankaccount'),
				'banknumber' => $this->input->post('banknumber'),
				'branch' => $this->input->post('branch'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->bank->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'bankname' => $this->input->post('bankname'),
				'bankaccount	' => $this->input->post('bankaccount'),
				'banknumber' => $this->input->post('banknumber'),
				'branch' => $this->input->post('branch'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->bank->update(array('idbank' => $this->input->post('idbank')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idbank)
	{
		$this->bank->delete_by_id($idbank);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idbank');
		foreach ($list_id as $idbank) {
			$this->bank->delete_by_id($idbank);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('bankname') == '')
		{
			$data['inputerror'][] = 'bankname';
			$data['error_string'][] = 'Bank name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
