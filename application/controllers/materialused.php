<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class materialused extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('template'));
		$this->load->model('user_model');
		$this->load->model('materialused_model','materialused');
		$this->load->model('matusedtype_model', 'matusedtype');
		$this->load->model('employee_model','employee'); 
		$this->load->model('warehouse_model','warehouse'); 
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		
		
		if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
		{
			redirect('web');
		}
		
	}

	//.........HEADER............
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('codetrans', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
		

			$this->load->helper('url');
			$this->template->display('materialused/index');
		}
		else
		{
			
			$userid= $this->session->userdata ( 'userId' );
			$codetrans = $this->input->post('codetrans', true);
			$query_str="INSERT INTO materialused (codetrans, notrans, datetrans, created_by, created_at)
			SELECT '".$codetrans."',
			IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(materialused.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
			FROM
			materialused
			WHERE codetrans= '".$codetrans."'";
			$this->db->query($query_str);

			$insert_id = $this->db->insert_id();

			redirect ('materialused/detail/'.$insert_id.'');
		}

	}

	public function ajax_list()
	{
		$list = $this->materialused->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $materialused) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$materialused->idtrans.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="materialused/detail/'.$materialused->idtrans.'" title="Edit")"> '.$materialused->notrans.'</a>';
			$row[] = $materialused->datetrans;
			$row[] = $materialused->warehousename;
			$row[] = $materialused->matusedtypename;
			$row[] = $materialused->employeename;
			$row[] = $materialused->remark; 

			if ($materialused->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Canceled </span>';
			};
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->materialused->count_all(),
			"recordsFiltered" => $this->materialused->count_filtered(),
			"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('matusedtypeid','Used Type','trim|required|max_length[128]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$query_str="SELECT
				materialused.idtrans,
				materialused.codetrans,
				materialused.notrans,
				DATE_FORMAT(
					materialused.datetrans,
					'%d/%m/%Y'
				) AS datetrans,
				materialused.matusedtypeid,
				matusedtype.matusedtypename,
				materialused.employeeid,
				employee.employeename,
				materialused.warehouseid,
				warehouse.warehousename,
				materialused.remark,
				CASE WHEN materialused.status=0 THEN 1 ELSE 0 END AS statussw,
				materialused.status
			FROM
				materialused
			LEFT JOIN matusedtype ON materialused.matusedtypeid = matusedtype.idmatusedtype
			LEFT JOIN employee ON materialused.employeeid = employee.idemployee
			LEFT JOIN warehouse ON materialused.warehouseid = warehouse.idwarehouse
			WHERE materialused.idtrans= ". $id ."";

			$data['detail'] = $this->db->query($query_str)->row_array();
			$data['matusedtype'] = $this->matusedtype->getdata()->result();
			$data['employee'] = $this->employee->getdata()->result(); 
			$data['warehouse'] = $this->warehouse->getdata()->result(); 

			$this->template->display('materialused/detail', $data);
		}
		else
		{

			$userid= $this->session->userdata ( 'userId' );
			$this->load->helper('date');

			$vardatetime = $this->input->post('datetrans', true); 
			$date = str_replace('/', '-', $vardatetime);
			$datetrans = date('Y-m-d', strtotime($date));

			$this->db->where('idtrans', $id);
			$this->db->update('materialused',array(
				'status'=>$this->input->post('status', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
				)
			);
			redirect ('materialused');
		}
	}

	public function saveprint()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$vardatetime = $this->input->post('datetrans', true); 
		$date = str_replace('/', '-', $vardatetime);
		$datetrans = date('Y-m-d', strtotime($date));

		$this->_validate();
		$data = array(
				'datetrans'=>$datetrans,
				'matusedtypeid'=>$this->input->post('matusedtypeid', true),
				'employeeid'=>$this->input->post('employeeid', true),
				'warehouseid'=>$this->input->post('warehouseid', true),  
				'remark'=>$this->input->post('remark', true),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->materialused->saveprint(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function canceltransaction()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');

		$this->_validate();
		$data = array( 
				'status'=>9,
				'deleted_by'=>$userid,
				'deleted_at'=>date('Y-m-d H:i:s'),
			);
		$this->materialused->canceltransaction(array('idtrans' => $this->input->post('idtrans')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_cancel()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->load->helper('date');
		//$this->materialused->canceltransaction($idtrans);

		$list_id = $this->input->post('idtrans');

		// foreach ($list_id as $idtrans) {
		// $data = array( 
		// 		'status'=>9,
		// 		'deleted_by'=>$userid,
		// 		'deleted_at'=>date('Y-m-d H:i:s'),
		// 	);
		
		// $this->materialused->delete_by_id($idtrans);
		// }
		// echo json_encode(array("status" => TRUE));

		// $list_id = $this->input->post('idshipto');
		foreach ($list_id as $idtrans) {
			$this->idtrans->delete_by_id($idtrans);
		}
		echo json_encode(array("status" => TRUE));

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('matusedtypeid') == '')
		{
			$data['inputerror'][] = 'matusedtypeid';
			$data['error_string'][] = 'Uded type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	//.........DETAIL............
	public function list_detail()
	{
		$this->load->helper('url');
		$segment = $this->uri->segment('3');
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'idtransdet';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

		$offset = ($page-1) * $rows;

		$result = array();


		$where = "IFNULL(inventory.inventoryname,'') like '%$msearchdetail%'";

		$rscount = "select count(*) AS total FROM
		materialuseddet
		INNER JOIN inventory ON materialuseddet.inventoryid = inventory.idinventory where " . $where;
		$rowcount =  $this->db->query($rscount)->result_array();

		$result = $rowcount[0];

		$query_str="SELECT
			materialuseddet.idtransdet,
			materialuseddet.transid,
			materialuseddet.inventoryid,
			inventory.inventorycode,
			inventory.inventoryname,
			materialuseddet.unit,
			materialuseddet.rackid,
			rack.rackname,
			FORMAT(materialuseddet.quantity,0) AS quantity,
			FORMAT(materialuseddet.appquantity,0) AS appquantity
		FROM
			materialuseddet
		LEFT JOIN inventory ON materialuseddet.inventoryid = inventory.idinventory
		LEFT JOIN rack ON materialuseddet.rackid = rack.idrack
		WHERE materialuseddet.transid = '".$segment."' AND " . $where . " order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idtransdet'=>$data['idtransdet'],
				'inventoryid'=>$data['inventoryid'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'], 
				'unit'=>$data['unit'], 
				'rackid'=>$data['rackid'],
				'rackname'=>$data['rackname'],
				'quantity'=>$data['quantity'],
				'appquantity'=>$data['appquantity']
				);
		}

		
		if ($row == null) {
			$result['rows'] = [];
		}else{
			$result['rows'] = $row;
		}	

		// SUM() for footer --------------------
		$rs = "SELECT FORMAT(SUM(quantity),0) AS quantity, FORMAT(SUM(appquantity),0) AS appquantity FROM
		materialuseddet
		INNER JOIN inventory ON materialuseddet.inventoryid = inventory.idinventory
		WHERE materialuseddet.transid = '".$segment."'";
		$criteria = $this->db->query($rs);

		$totalitems = array();

		foreach($criteria->result_array() as $data)
		{	
			$rowtotal[] = array('rackname'=>'Total', 
				'quantity'=>$data['quantity'],
				'appquantity'=>$data['appquantity']
				);
		}

		$result["footer"] = $rowtotal;
		echo json_encode($result);
	}

	public function create_detail()
	{
		if(!isset($_POST))	
			show_404();
		$segment = $this->uri->segment('3');
		if($this->materialused->create_detail($segment))
			
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Create data failed'));
	}
	
	public function update_detail($id=null)
	{

		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['idtransdet']));

		if($this->materialused->update_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Update data failed'));

	}
	
	public function delete_detail()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->materialused->delete_detail($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Delete data failed'));
	}

}
