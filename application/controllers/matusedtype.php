<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class matusedtype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('matusedtype_model','matusedtype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('matusedtype/index');
	}

	public function ajax_list()
	{
		$list = $this->matusedtype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $matusedtype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$matusedtype->idmatusedtype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$matusedtype->idmatusedtype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$matusedtype->idmatusedtype."'".')"> | Delete</a>';
			$row[] = $matusedtype->matusedtypename;

			if ($matusedtype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->matusedtype->count_all(),
						"recordsFiltered" => $this->matusedtype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idmatusedtype)
	{
		$data = $this->matusedtype->get_by_id($idmatusedtype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'matusedtypename' => $this->input->post('matusedtypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->matusedtype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'matusedtypename' => $this->input->post('matusedtypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->matusedtype->update(array('idmatusedtype' => $this->input->post('idmatusedtype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idmatusedtype)
	{
		$this->matusedtype->delete_by_id($idmatusedtype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idmatusedtype');
		foreach ($list_id as $idmatusedtype) {
			$this->matusedtype->delete_by_id($idmatusedtype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('matusedtypename') == '')
		{
			$data['inputerror'][] = 'matusedtypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
