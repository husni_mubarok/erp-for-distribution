<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class combogrid extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('combogrid_model','combogrid');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }   	 
	}

	public function inventory()
	{
			echo $this->combogrid->getInventory();
	}

	public function account()
	{
			echo $this->combogrid->getAccount();
	}

	public function employee()
	{
			echo $this->combogrid->getEmployee();
	}
}
