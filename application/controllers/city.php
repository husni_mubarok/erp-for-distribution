<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class city extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('city_model','city');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('city/index');
	}

	public function ajax_list()
	{
		$list = $this->city->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $city) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$city->idcity.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$city->idcity."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$city->idcity."'".')"> | Delete</a>';
			$row[] = $city->cityname;

			if ($city->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->city->count_all(),
						"recordsFiltered" => $this->city->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcity)
	{
		$data = $this->city->get_by_id($idcity);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'cityname' => $this->input->post('cityname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->city->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'cityname' => $this->input->post('cityname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->city->update(array('idcity' => $this->input->post('idcity')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcity)
	{
		$this->city->delete_by_id($idcity);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcity');
		foreach ($list_id as $idcity) {
			$this->city->delete_by_id($idcity);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('cityname') == '')
		{
			$data['inputerror'][] = 'cityname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
