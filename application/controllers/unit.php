
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class unit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('unit_model','unit');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('unit/index');
	}

	public function ajax_list()
	{
		$list = $this->unit->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $unit) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$unit->idunit.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$unit->idunit."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$unit->idunit."'".')"> | Delete</a>';
			$row[] = $unit->unitname;

			if ($unit->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->unit->count_all(),
						"recordsFiltered" => $this->unit->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idunit)
	{
		$data = $this->unit->get_by_id($idunit);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'unitname' => $this->input->post('unitname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->unit->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'unitname' => $this->input->post('unitname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->unit->update(array('idunit' => $this->input->post('idunit')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idunit)
	{
		$this->unit->delete_by_id($idunit);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idunit');
		foreach ($list_id as $idunit) {
			$this->unit->delete_by_id($idunit);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('unitname') == '')
		{
			$data['inputerror'][] = 'unitname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
