<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class costcenter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('costcenter_model','costcenter');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('costcenter/index');
	}

	public function ajax_list()
	{
		$list = $this->costcenter->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $costcenter) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$costcenter->idcostcenter.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$costcenter->idcostcenter."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$costcenter->idcostcenter."'".')"> | Delete</a>';
			$row[] = $costcenter->costcentername;

			if ($costcenter->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->costcenter->count_all(),
						"recordsFiltered" => $this->costcenter->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcostcenter)
	{
		$data = $this->costcenter->get_by_id($idcostcenter);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'costcentername' => $this->input->post('costcentername'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->costcenter->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'costcentername' => $this->input->post('costcentername'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->costcenter->update(array('idcostcenter' => $this->input->post('idcostcenter')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcostcenter)
	{
		$this->costcenter->delete_by_id($idcostcenter);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcostcenter');
		foreach ($list_id as $idcostcenter) {
			$this->costcenter->delete_by_id($idcostcenter);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('costcentername') == '')
		{
			$data['inputerror'][] = 'costcentername';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
