<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class depreciationtype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('depreciationtype_model','depreciationtype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('depreciationtype/index');
	}

	public function ajax_list()
	{
		$list = $this->depreciationtype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $depreciationtype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$depreciationtype->iddepreciationtype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$depreciationtype->iddepreciationtype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$depreciationtype->iddepreciationtype."'".')"> | Delete</a>';
			$row[] = $depreciationtype->depreciationtypename;

			if ($depreciationtype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->depreciationtype->count_all(),
						"recordsFiltered" => $this->depreciationtype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($iddepreciationtype)
	{
		$data = $this->depreciationtype->get_by_id($iddepreciationtype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'depreciationtypename' => $this->input->post('depreciationtypename'),
				'status' => $this->input->post('status'),
			);
		$insert = $this->depreciationtype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'depreciationtypename' => $this->input->post('depreciationtypename'),
				'status' => $this->input->post('status'),
			);
		$this->depreciationtype->update(array('iddepreciationtype' => $this->input->post('iddepreciationtype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($iddepreciationtype)
	{
		$this->depreciationtype->delete_by_id($iddepreciationtype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('iddepreciationtype');
		foreach ($list_id as $iddepreciationtype) {
			$this->depreciationtype->delete_by_id($iddepreciationtype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('depreciationtypename') == '')
		{
			$data['inputerror'][] = 'depreciationtypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
