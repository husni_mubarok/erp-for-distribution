<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('supplier_model','supplier');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('supplier/index');
	}

	public function ajax_list()
	{
		$list = $this->supplier->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $supplier) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$supplier->idsupplier.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$supplier->idsupplier."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$supplier->idsupplier."'".')"> | Delete</a>';
			$row[] = $supplier->suppliercode;
			$row[] = $supplier->suppliername;
			$row[] = $supplier->phone1;
			$row[] = $supplier->hp1;
			$row[] = $supplier->emailaddress;
			$row[] = $supplier->website;
			$row[] = $supplier->fax;
			$row[] = $supplier->address1;
			$row[] = $supplier->reknumber;
			$row[] = $supplier->zipcode;

			if ($supplier->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->supplier->count_all(),
						"recordsFiltered" => $this->supplier->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idsupplier)
	{
		$data = $this->supplier->get_by_id($idsupplier);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'suppliercode' => $this->input->post('suppliercode'),
				'suppliername' => $this->input->post('suppliername'),
				'phone1' => $this->input->post('phone1'),
				'hp1' => $this->input->post('hp1'),
				'emailaddress' => $this->input->post('emailaddress'),
				'fax' => $this->input->post('fax'),
				'address1' => $this->input->post('address1'),
				'reknumber' => $this->input->post('reknumber'),
				'zipcode' => $this->input->post('zipcode'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->supplier->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'suppliercode' => $this->input->post('suppliercode'),
				'suppliername' => $this->input->post('suppliername'),
				'phone1' => $this->input->post('phone1'),
				'hp1' => $this->input->post('hp1'),
				'emailaddress' => $this->input->post('emailaddress'),
				'fax' => $this->input->post('fax'),
				'address1' => $this->input->post('address1'),
				'reknumber' => $this->input->post('reknumber'),
				'zipcode' => $this->input->post('zipcode'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->supplier->update(array('idsupplier' => $this->input->post('idsupplier')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idsupplier)
	{
		$this->supplier->delete_by_id($idsupplier);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idsupplier');
		foreach ($list_id as $idsupplier) {
			$this->supplier->delete_by_id($idsupplier);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('suppliername') == '')
		{
			$data['inputerror'][] = 'suppliername';
			$data['error_string'][] = 'Supplier name is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
