<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventorybrand extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('inventorybrand_model','inventorybrand');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('inventorybrand/index');
	}

	public function ajax_list()
	{
		$list = $this->inventorybrand->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $inventorybrand) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$inventorybrand->idinventorybrand.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$inventorybrand->idinventorybrand."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$inventorybrand->idinventorybrand."'".')"> | Delete</a>';
			$row[] = $inventorybrand->inventorybrandname;

			if ($inventorybrand->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->inventorybrand->count_all(),
						"recordsFiltered" => $this->inventorybrand->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idinventorybrand)
	{
		$data = $this->inventorybrand->get_by_id($idinventorybrand);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorybrandname' => $this->input->post('inventorybrandname'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->inventorybrand->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'inventorybrandname' => $this->input->post('inventorybrandname'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->inventorybrand->update(array('idinventorybrand' => $this->input->post('idinventorybrand')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idinventorybrand)
	{
		$this->inventorybrand->delete_by_id($idinventorybrand);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idinventorybrand');
		foreach ($list_id as $idinventorybrand) {
			$this->inventorybrand->delete_by_id($idinventorybrand);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('inventorybrandname') == '')
		{
			$data['inputerror'][] = 'inventorybrandname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
