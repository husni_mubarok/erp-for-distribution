<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class rack extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('rack_model','rack');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('rack/index');
	}

	public function ajax_list()
	{
		$list = $this->rack->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rack) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$rack->idrack.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$rack->idrack."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$rack->idrack."'".')"> | Delete</a>';
			$row[] = $rack->rackname;
			$row[] = $rack->lane;
			$row[] = $rack->floor;

			if ($rack->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->rack->count_all(),
						"recordsFiltered" => $this->rack->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idrack)
	{
		$data = $this->rack->get_by_id($idrack);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'rackname' => $this->input->post('rackname'),
				'lane' => $this->input->post('lane'),
				'floor' => $this->input->post('floor'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->rack->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'rackname' => $this->input->post('rackname'),
				'lane' => $this->input->post('lane'),
				'floor' => $this->input->post('floor'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->rack->update(array('idrack' => $this->input->post('idrack')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idrack)
	{
		$this->rack->delete_by_id($idrack);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idrack');
		foreach ($list_id as $idrack) {
			$this->rack->delete_by_id($idrack);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('rackname') == '')
		{
			$data['inputerror'][] = 'rackname';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
