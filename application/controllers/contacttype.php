<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class contacttype extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('template'));
		$this->load->model('contacttype_model','contacttype');
		
		$isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('web');
        }
		
	}

	public function index()
	{
		$this->load->helper('url');
		$this->template->display('contacttype/index');
	}

	public function ajax_list()
	{
		$list = $this->contacttype->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $contacttype) {
			$no++;
			$row = array();
			$row[] = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$contacttype->idcontacttype.'"> <div class="control__indicator"></div> </label>';
			//add html for action
			$row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$contacttype->idcontacttype."'".')"> Edit</a>
				  <a href="javascript:void(0)" title="Hapus" onclick="delete_id('."'".$contacttype->idcontacttype."'".')"> | Delete</a>';
			$row[] = $contacttype->contacttypename;

			if ($contacttype->status == 0) {
				$row[] = '<span class="label label-success"> Active </span>';
			}else{
				$row[] = '<span class="label label-danger"> Not Active </span>';
			};
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->contacttype->count_all(),
						"recordsFiltered" => $this->contacttype->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($idcontacttype)
	{
		$data = $this->contacttype->get_by_id($idcontacttype);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'contacttypename' => $this->input->post('contacttypename'),
				'status' => $this->input->post('status'),
				'created_by'=>$userid,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		$insert = $this->contacttype->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$userid= $this->session->userdata ( 'userId' );
		$this->_validate();
		$data = array(
				'contacttypename' => $this->input->post('contacttypename'),
				'status' => $this->input->post('status'),
				'updated_by'=>$userid,
				'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->contacttype->update(array('idcontacttype' => $this->input->post('idcontacttype')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idcontacttype)
	{
		$this->contacttype->delete_by_id($idcontacttype);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_bulk_delete()
	{
		$list_id = $this->input->post('idcontacttype');
		foreach ($list_id as $idcontacttype) {
			$this->contacttype->delete_by_id($idcontacttype);
		}
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('contacttypename') == '')
		{
			$data['inputerror'][] = 'contacttypename';
			$data['error_string'][] = 'Expedition type is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
