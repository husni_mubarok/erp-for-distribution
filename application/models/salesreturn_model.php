<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class salesreturn_model extends CI_Model {

//data header
	var $table = 'salesreturn';
	var $column_order = array('codetrans','codetrans','notrans','datetrans','customername','currency','top','orderref','paytypename',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','customername','currency','top','orderref','paytypename'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('salesreturn.idtrans,
			salesreturn.codetrans,
			salesreturn.notrans,
			DATE_FORMAT(salesreturn.datetrans,"%d/%m/%Y") AS datetrans,
			salesreturn.customerid,
			customer.customername,
			salesreturn.currency,
			salesreturn.top,
			salesreturn.orderref,
			salesreturn.status,
							paytype.paytypename', FALSE); // <-- There is never any reason to write this line!
		$this->db->from('salesreturn');
		$this->db->join('customer','salesreturn.customerid = customer.idcustomer', 'left');
		$this->db->join('paytype','salesreturn.paytypeid = paytype.idpaytype', 'left');

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idsalesreturn)
	{
		$this->db->from($this->table);
		$this->db->where('idsalesreturn',$idsalesreturn);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	//DETAIL............

	public function create_detail($segment)
	{
		$segment = $this->uri->segment(3);
		//$segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;   
		return $this->db->insert('salesreturndet',array(
			'transid'=>$segment,
			'inventoryid'=>$this->input->post('inventoryid',true),
			'unitprice'=>$this->input->post('unitprice',true),
			'unit'=>$this->input->post('unit',true),
			'discount'=>$this->input->post('discount',true),
			'amount'=>$this->input->post('amount',true),
			'quantity'=>$this->input->post('quantity',true)
			));
	}
	
	public function update_detail($id)
	{
		$this->db->where('idtransdet', $id);
		return $this->db->update('salesreturndet',array(
			'inventoryid'=>$this->input->post('inventoryid',true),
			'unitprice'=>$this->input->post('unitprice',true),
			'unit'=>$this->input->post('unit',true),
			'discount'=>$this->input->post('discount',true),
			'amount'=>$this->input->post('amount',true),
			'quantity'=>$this->input->post('quantity',true)
			));
	}
	
	public function delete_detail($id)
	{
		return $this->db->delete('salesreturndet', array('idtransdet' => $id)); 
	}

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('salesreturn',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
