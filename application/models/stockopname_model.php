<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class stockopname_model extends CI_Model {

//data header
	var $table = 'stockopname';
	var $column_order = array('codetrans','codetrans','notrans','datetrans','matusedtypename','employeename',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','matusedtypename','employeename'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('stockopname.idtrans, 
			DATE_FORMAT(stockopname.datetrans,"%d/%m/%Y") AS datetrans,
			stockopname.notrans,
			stockopname.datetrans,
			stockopname.matusedtypeid,
			stockopname.remark,
			stockopname.`status`,
			stockopname.warehouseid,
			warehouse.warehousecode,
			warehouse.warehousename,
			matusedtype.matusedtypename,
			stockopname.status', FALSE); // <-- There is never any reason to write this line!
		$this->db->from('stockopname');
		$this->db->join('matusedtype','stockopname.matusedtypeid = matusedtype.idmatusedtype', 'left');
		$this->db->join('warehouse','stockopname.warehouseid = warehouse.idwarehouse', 'left');

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idstockopname)
	{
		$this->db->from($this->table);
		$this->db->where('idstockopname',$idstockopname);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	//DETAIL............

	public function create_detail($segment)
	{
		$segment = $this->uri->segment(3);
		//$segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;   
		return $this->db->insert('stockopnamedet',array(
			'transid'=>$segment,
			'inventoryid'=>$this->input->post('inventoryid',true), 
			'unit'=>$this->input->post('unit',true),  
			'quantity'=>$this->input->post('quantity',true),
			'stockqty'=>$this->input->post('stockqty',true),
			'adjqty'=>$this->input->post('adjqty',true)
			));
	}
	
	public function update_detail($id)
	{
		$this->db->where('idtransdet', $id);
		return $this->db->update('stockopnamedet',array(
			'inventoryid'=>$this->input->post('inventoryid',true), 
			'unit'=>$this->input->post('unit',true), 
			'quantity'=>$this->input->post('quantity',true),
			'stockqty'=>$this->input->post('stockqty',true),
			'adjqty'=>$this->input->post('adjqty',true)
			));
	}
	
	public function delete_detail($id)
	{
		return $this->db->delete('stockopnamedet', array('idtransdet' => $id)); 
	}

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('stockopname',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
