<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class arbeginning_model extends CI_Model {

//data header
	var $table = 'arinvoice';
	var $column_order = array('codetrans','notrans','datetrans',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('arinvoice.idtrans,
							arinvoice.codetrans,
							arinvoice.notrans,
							DATE_FORMAT(
								arinvoice.datetrans,
								"%d/%m/%Y"
							) AS datetrans,
							arinvoice.amount,
							arinvoice.remark,
							arinvoice.`status`,
							arinvoice.customerid,
							customer.customername,
							arinvoice.duedate,
							arinvoice.top,
							arinvoice.currency,
							arinvoice.beginamt,
							arinvoice.settleamt,
							arinvoice.remainamt,
							arinvoice.invoiceref', FALSE);
		$this->db->from('arinvoice');
		$this->db->join('customer', 'arinvoice.customerid = customer.idcustomer', 'left'); 
 		
		//print_r($this->db);

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idarinvoice)
	{
		$this->db->from($this->table);
		$this->db->where('idarinvoice',$idarinvoice);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	} 

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('arinvoice',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
