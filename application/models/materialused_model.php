<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class materialused_model extends CI_Model {

//data header
	var $table = 'materialused';
	var $column_order = array('codetrans','codetrans','notrans','datetrans','matusedtypename','employeename',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','matusedtypename','employeename'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('materialused.idtrans,
			materialused.codetrans,
			materialused.notrans,
			DATE_FORMAT(materialused.datetrans,"%d/%m/%Y") AS datetrans,
			materialused.matusedtypeid,
			matusedtype.matusedtypename,
			materialused.employeeid,
			employee.employeename,
			materialused.warehouseid,
			warehouse.warehousename,
			materialused.remark,
			materialused.status', FALSE); // <-- There is never any reason to write this line!
		$this->db->from('materialused');
		$this->db->join('matusedtype','materialused.matusedtypeid = matusedtype.idmatusedtype', 'left');
		$this->db->join('employee','materialused.employeeid = employee.idemployee', 'left');
		$this->db->join('warehouse','materialused.warehouseid = warehouse.idwarehouse', 'left');

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idmaterialused)
	{
		$this->db->from($this->table);
		$this->db->where('idmaterialused',$idmaterialused);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	//DETAIL............

	public function create_detail($segment)
	{
		$segment = $this->uri->segment(3);
		//$segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;   
		return $this->db->insert('materialuseddet',array(
			'transid'=>$segment,
			'inventoryid'=>$this->input->post('inventoryid',true), 
			'unit'=>$this->input->post('unit',true),  
			'rackid'=>$this->input->post('rackid',true),
			'quantity'=>$this->input->post('quantity',true),
			'appquantity'=>$this->input->post('appquantity',true)
			));
	}
	
	public function update_detail($id)
	{
		$this->db->where('idtransdet', $id);
		return $this->db->update('materialuseddet',array(
			'inventoryid'=>$this->input->post('inventoryid',true), 
			'unit'=>$this->input->post('unit',true), 
			'rackid'=>$this->input->post('rackid',true),
			'quantity'=>$this->input->post('quantity',true),
			'appquantity'=>$this->input->post('appquantity',true)
			));
	}
	
	public function delete_detail($id)
	{
		return $this->db->delete('materialuseddet', array('idtransdet' => $id)); 
	}

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('materialused',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
