<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class combogrid_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getInventory()
	{
		
		$q = isset($_POST['q']) ? strval($_POST['q']) : '';

		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$msearchinv = isset($_POST['msearchinv']) ? mysql_real_escape_string($_POST['msearchinv']) : '';
		// $result = array();

		$offset = ($page-1)*$rows;

		$result = array();

		$where = "inventoryname like '%$msearchinv%'";
		$query_str_count="select count(*) from inventory";
		$row =  $this->db->query($query_str_count)->result_array();
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'inventoryname';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$result["total"] = $row[0];

		$query_str="SELECT
		inventory.inventoryname,
		inventory.inventorycode,
		inventory.salesunit,
		inventory.idinventory,
		FORMAT(inventory.unitprice,0) AS unitprice
		from inventory WHERE inventoryname like '%$q%' or inventorycode like '%$q%' order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idinventory'=>$data['idinventory'],
				'inventorycode'=>$data['inventorycode'],
				'inventoryname'=>$data['inventoryname'],
				'salesunit'=>$data['salesunit'],
				'unitprice'=>$data['unitprice']
				);
		}

		echo json_encode($row);
	}

	public function getAccount()
	{
		
		$q = isset($_POST['q']) ? strval($_POST['q']) : '';

		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$msearchinv = isset($_POST['msearchinv']) ? mysql_real_escape_string($_POST['msearchinv']) : '';
		// $result = array();

		$offset = ($page-1)*$rows;

		$result = array();

		$where = "accountname like '%$msearchinv%'";
		$query_str_count="select count(*) from account";
		$row =  $this->db->query($query_str_count)->result_array();
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'accountname';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$result["total"] = $row[0];

		$query_str="SELECT
		account.accountname,
		account.accountcode,
		account.idaccount 
		from account WHERE accountname like '%$q%' or accountcode like '%$q%' order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idaccount'=>$data['idaccount'],
				'accountcode'=>$data['accountcode'],
				'accountname'=>$data['accountname'] 
				);
		}

		echo json_encode($row);
	}

public function getEmployee()
	{
		
		$q = isset($_POST['q']) ? strval($_POST['q']) : '';

		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$msearchinv = isset($_POST['msearchinv']) ? mysql_real_escape_string($_POST['msearchinv']) : '';
		// $result = array();

		$offset = ($page-1)*$rows;

		$result = array();

		$where = "employeename like '%$msearchinv%'";
		$query_str_count="select count(*) FROM
						employee
					LEFT JOIN position ON employee.positionid = position.idposition
					LEFT JOIN department ON employee.departmentid = department.iddepartment";
		$row =  $this->db->query($query_str_count)->result_array();
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'employeename';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$result["total"] = $row[0];

		$query_str="SELECT
						employee.idemployee,
						employee.nik,
						employee.identityno,
						employee.employeename,
						employee.address,
						employee.maritalstatus,
						position.positionname,
						employee.positionid,
						employee.departmentid,
						department.departmentname
					FROM
						employee
					LEFT JOIN position ON employee.positionid = position.idposition
					LEFT JOIN department ON employee.departmentid = department.iddepartment
					WHERE employeename like '%$q%' or nik like '%$q%' order by $sort $order limit $offset,$rows";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idemployee'=>$data['idemployee'],
				'nik'=>$data['nik'],
				'employeename'=>$data['employeename'],
				'positionname'=>$data['positionname'],
				'departmentname'=>$data['departmentname'] 
				);
		}

		echo json_encode($row);
	}

}

