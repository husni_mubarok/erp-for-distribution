<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class prepaid_model extends CI_Model {

//data header
	var $table = 'prepaid';
	var $column_order = array('codetrans','notrans','datetrans','suppliername','currency','top','orderref','paytypename',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','suppliername','currency','top','orderref','paytypename'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('prepaid.idtrans,
						prepaid.codetrans,
						prepaid.notrans,
						prepaid.datetrans,
						prepaid.prepaidname,
						prepaid.accountid,
						account.accountname,
						prepaid.cost,
						prepaid.usefullife,
						prepaid.beginamount,
						prepaid.remark,
						prepaid.status', FALSE);
		$this->db->from('prepaid');
		$this->db->join('account', 'prepaid.accountid = account.idaccount', 'left'); 
 
		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idprepaid)
	{
		$this->db->from($this->table);
		$this->db->where('idprepaid',$idprepaid);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	//DETAIL............

	public function create_detail($segment)
	{
		$segment = $this->uri->segment(3);
		//$segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;   
		return $this->db->insert('prepaiddet',array(
			'transid'=>$segment,
			'accountid'=>$this->input->post('accountid',true),
			'costcenterid'=>$this->input->post('costcenterid',true),
			'description'=>$this->input->post('description',true),
			'debt'=>$this->input->post('debt',true),
			'credit'=>$this->input->post('credit',true) 
			));
	}
	
	public function update_detail($id)
	{
		$this->db->where('idtransdet', $id);
		return $this->db->update('prepaiddet',array(
			'accountid'=>$this->input->post('accountid',true),
			'costcenterid'=>$this->input->post('costcenterid',true),
			'description'=>$this->input->post('description',true),
			'debt'=>$this->input->post('debt',true),
			'credit'=>$this->input->post('credit',true) 
			));
	}
	
	public function delete_detail($id)
	{
		return $this->db->delete('prepaiddet', array('idtransdet' => $id)); 
	}

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('prepaid',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
