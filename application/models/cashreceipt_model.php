<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cashreceipt_model extends CI_Model {

//data header
	var $table = 'cashreceipt';
	var $column_order = array('codetrans','notrans','datetrans','customername','currency','top','orderref','paytypename',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','customername','currency','top','orderref','paytypename'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('cashreceipt.idtrans,
						DATE_FORMAT(cashreceipt.datetrans,"%d/%m/%Y") AS datetrans,
						cashreceipt.notrans,
						cashreceipt.customerid,
						customer.customername,
						paytype.paytypename,
						currency.currencyname,
						cashreceipt.paytypeid,
						cashreceipt.currencyid,
						cashreceipt.accountcashid,
						account.accountname,
						cashreceipt.amount,
						cashreceipt.remark,
						cashreceipt.status', FALSE);
		$this->db->from('cashreceipt');
		$this->db->join('customer', 'cashreceipt.customerid = customer.idcustomer', 'left');
		$this->db->join('paytype', 'cashreceipt.paytypeid = paytype.idpaytype', 'left');
		$this->db->join('account', 'cashreceipt.accountcashid = account.idaccount', 'left');
		$this->db->join('currency', 'currency.idcurrency = cashreceipt.currencyid', 'left');
 
		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idcashreceipt)
	{
		$this->db->from($this->table);
		$this->db->where('idcashreceipt',$idcashreceipt);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	//DETAIL............

	public function create_detail($segment)
	{
		$segment = $this->uri->segment(3);
		//$segment = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;   
		return $this->db->insert('cashreceiptdet',array(
			'transid'=>$segment,
			'accountid'=>$this->input->post('accountid',true),
			'costcenterid'=>$this->input->post('costcenterid',true),
			'costitemid'=>$this->input->post('costitemid',true),
			'debt'=>$this->input->post('debt',true),
			'credit'=>$this->input->post('credit',true),
			'amount'=>$this->input->post('amount',true)
			));
	}
	
	public function update_detail($id)
	{
		$this->db->where('idtransdet', $id);
		return $this->db->update('cashreceiptdet',array(
			'accountid'=>$this->input->post('accountid',true),
			'costcenterid'=>$this->input->post('costcenterid',true),
			'costitemid'=>$this->input->post('costitemid',true),
			'debt'=>$this->input->post('debt',true),
			'credit'=>$this->input->post('credit',true),
			'amount'=>$this->input->post('amount',true)
			));
	}
	
	public function delete_detail($id)
	{
		return $this->db->delete('cashreceiptdet', array('idtransdet' => $id)); 
	}

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('cashreceipt',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
