<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class inventory_model extends CI_Model {

	var $table = 'inventory';
	var $column_order = array('inventorycode','inventoryname','spec','inventorybrandcode','inventorygroupid','barcode','weight','inventorytypeid','unitprice','floorprice','inventorybrandid','inventorycolorid','inventorysizeid','inventorybrandname','inventorycolorname','inventorygroupname','inventorytypename','status',null); //set column field database for datatable orderable
	var $column_search = array('inventorycode','inventoryname','spec','inventorybrandcode','inventorygroupid','barcode','weight','inventorytypeid','unitprice','floorprice','inventorybrandid','inventorycolorid','inventorysizeid'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('idinventory' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		//$this->db->from($this->table);
		$this->db->select('inventory.idinventory,
							inventory.inventorycode,
							inventory.inventoryname,
							inventory.spec,
							inventory.inventorybrandcode,
							inventory.inventorygroupid,
							inventory.barcode,
							inventory.weight,
							inventory.inventorytypeid,
							inventory.unitprice,
							inventory.floorprice,
							inventory.inventorybrandid,
							inventory.inventorytypeid,
							inventory.inventorycolorid,
							inventory.inventorysizeid,
							inventory.status,
							inventorybrand.inventorybrandname,
							inventorycolor.inventorycolorname,
							inventorygroup.inventorygroupname,
							inventorytype.inventorytypename',

							FALSE); // <-- There is never any reason to write this line!
		$this->db->from('inventory');
		$this->db->join('inventorybrand','inventory.inventorybrandid = inventorybrand.idinventorybrand', 'left');
		$this->db->join('inventorycolor','inventory.inventorycolorid = inventorycolor.idinventorycolor', 'left');
		$this->db->join('inventorygroup','inventory.inventorygroupid = inventorygroup.idinventorygroup', 'left');
		$this->db->join('inventorytype','inventory.inventorytypeid = inventorytype.idinventorytype', 'left');

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					//$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				//if(count($this->column_search) - 1 == $i) //last loop
				//	$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idinventory)
	{
		$this->db->from($this->table);
		$this->db->where('idinventory',$idinventory);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($idinventory)
	{
		$this->db->where('idinventory', $idinventory);
		$this->db->delete($this->table);
	}

	function getdata(){
        return $this->db->get("inventory");
    }


}
