<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cashtransfer_model extends CI_Model {

//data header
	var $table = 'cashtransfer';
	var $column_order = array('codetrans','notrans','datetrans','accountfrom','accountto',null); //set column field database for datatable orderable
	var $column_search = array('codetrans','notrans','datetrans','accountfrom','accountto'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	//var $order = array('idtrans' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select('cashtransfer.idtrans,
						cashtransfer.codetrans,
						cashtransfer.notrans, 
						DATE_FORMAT(cashtransfer.datetrans,"%d/%m/%Y") AS datetrans,
						cashtransfer.acccashfromid,
						account.accountname AS accountfrom,
						cashtransfer.acccashtoid,
						accounto.accountname AS accountto,
						cashtransfer.amount,
						cashtransfer.rate,
						cashtransfer.charge,
						cashtransfer.approveddate,
						cashtransfer.remark,
						cashtransfer.status', FALSE);
		$this->db->from('cashtransfer');
		$this->db->join('account', 'cashtransfer.acccashfromid = account.idaccount', 'left');
		$this->db->join('account AS accounto', 'cashtransfer.acccashtoid = accounto.idaccount', 'left'); 
 		
		//print_r($this->db);

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->like($item, $_POST['search']['value']);
				}
				else 
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idcashtransfer)
	{
		$this->db->from($this->table);
		$this->db->where('idcashtransfer',$idcashtransfer);
		$query = $this->db->get();

		return $query->row();
	}

	public function saveprint($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function canceltransaction($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function cancel_by_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	} 

	function update_header($id, $data){
		$this->db->where('idtrans', $id);
        $this->db->update('cashtransfer',$data);
    }

    public function delete_by_id($istrans)
	{
		$this->db->where('istrans', $istrans);
		$this->db->delete($this->table);
	}

}
