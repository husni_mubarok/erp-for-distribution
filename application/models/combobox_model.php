<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class combobox_model extends CI_Model {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

   public function getUnit()
	{
		
		$result = array();
		$query_str="SELECT
					*
					FROM
					unit";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idunit'=>$data['idunit'],
				'unitname'=>$data['unitname']
			);
		}
		return json_encode($row);
	}

	public function getRack()
	{
		
		$result = array();
		$query_str="SELECT
					*
					FROM
					rack";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idrack'=>$data['idrack'],
				'rackname'=>$data['rackname']
			);
		}
		return json_encode($row);
	}

	public function getCostcenter()
	{
		
		$result = array();
		$query_str="SELECT
					*
					FROM
					costcenter";
		$criteria = $this->db->query($query_str);
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'idcostcenter'=>$data['idcostcenter'],
				'costcentername'=>$data['costcentername']
			);
		}
		return json_encode($row);
	}

}

